# Privacy Policy

## Data Protection

<p>As an application provider, we take the protection of all personal data very seriously.
All personal information is treated confidentially and in accordance with the legal requirements,
regulations, as explained in this privacy policy.</p>
<p>This app is designed so that the user do not have to enter any personal data. Never will data
collected by us, and especially not passed to third parties. The users behaviour is also not
analyzed by this application.</p>
<p>The user is responsible what kind of data is added or retrieved from the IPFS network.
This kind of information is also not tracked by this application.</p>

## Android Permissions

<p>This section describes briefly why specific Android permissions are required.</p>
<ul>
<li>
<h4>Camera</h4>
<p>The camera permission is required to read URI-QR codes, which contains
information about the content data.
</p>
</li>
<li>
<h4>Foreground Service</h4>
<p>The foreground service permission is required to run the IPFS node over a longer period of time.
</p>
</li>
</ul>

### Contact Information
<p>Remmer Wilts, Dr. Munderloh Str.10, 27798 Wüsting, Germany</p>