package threads.server.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.WorkManager;

import java.util.List;

import threads.lite.core.Server;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.work.AutonatWorker;

public class DaemonService extends Service {

    private static final String ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE";
    private static final String ACTION_START_SERVICE = "ACTION_START_SERVICE";


    private static final String TAG = DaemonService.class.getSimpleName();

    private ConnectivityManager.NetworkCallback networkCallback;

    public static void start(@NonNull Context context) {
        try {
            Intent intent = new Intent(context, DaemonService.class);
            intent.setAction(ACTION_START_SERVICE);
            context.startForegroundService(intent);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void unRegisterNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            connectivityManager.unregisterNetworkCallback(networkCallback);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            EVENTS events = EVENTS.getInstance(getApplicationContext());
            networkCallback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    AutonatWorker.autonat(getApplicationContext());
                    events.online();
                }

                @Override
                public void onLost(Network network) {
                    try {
                        DOCS.getInstance(getApplicationContext())
                                .setReachability(DOCS.Reachability.UNKNOWN);
                        events.offline();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            };


            connectivityManager.registerDefaultNetworkCallback(networkCallback);

            if (threads.lite.cid.Network.isNetworkConnected(getApplicationContext())) {
                AutonatWorker.autonat(getApplicationContext());
                events.online();
            } else {
                events.offline();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction().equals(ACTION_START_SERVICE)) {
            buildNotification();
            registerNetworkCallback();
        } else if (intent.getAction().equals(ACTION_STOP_SERVICE)) {
            try {
                stopForeground(STOP_FOREGROUND_REMOVE);
            } finally {
                stopSelf();
            }
        }

        return START_NOT_STICKY;
    }

    private void buildNotification() {
        try {
            Notification.Builder builder = new Notification.Builder(
                    getApplicationContext(), InitApplication.DAEMON_CHANNEL_ID);

            Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
            int viewID = (int) System.currentTimeMillis();
            PendingIntent viewIntent = PendingIntent.getActivity(getApplicationContext(),
                    viewID, notifyIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);


            Intent stopIntent = new Intent(getApplicationContext(), DaemonService.class);
            stopIntent.setAction(ACTION_STOP_SERVICE);
            int requestID = (int) System.currentTimeMillis();
            PendingIntent stopPendingIntent = PendingIntent.getService(
                    getApplicationContext(), requestID, stopIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause),
                    getString(R.string.shutdown),
                    stopPendingIntent).build();

            builder.setSmallIcon(R.drawable.access_point_network);
            builder.addAction(action);
            builder.setContentTitle(getString(R.string.application_is_running));
            builder.setContentIntent(viewIntent);
            builder.setUsesChronometer(true);
            builder.setCategory(Notification.CATEGORY_SERVICE);
            builder.setOnlyAlertOnce(true);

            Notification notification = builder.build();
            startForeground(TAG.hashCode(), notification);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            // cancel all works
            WorkManager.getInstance(getApplicationContext()).cancelAllWork();

            unRegisterNetworkCallback();

            // closing app
            DOCS docs = DOCS.getInstance(getApplicationContext());
            Server server = docs.getServer();
            server.shutdown();
            removeFromResents();

            System.exit(0);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void removeFromResents() {
        try {
            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            if (am != null) {
                List<ActivityManager.AppTask> tasks = am.getAppTasks();
                if (tasks != null && tasks.size() > 0) {
                    tasks.get(0).setExcludeFromRecents(true);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
