package threads.server;


import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ListPopupWindow;
import androidx.appcompat.widget.SearchView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.slidingpanelayout.widget.SlidingPaneLayout;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.divider.MaterialDivider;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.books.BOOKS;
import threads.server.core.books.Bookmark;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.fragments.AccessFragment;
import threads.server.fragments.BookmarksDialogFragment;
import threads.server.fragments.BrowserFragment;
import threads.server.fragments.ContentDialogFragment;
import threads.server.fragments.FilesFragment;
import threads.server.fragments.NewFolderDialogFragment;
import threads.server.fragments.SettingsDialogFragment;
import threads.server.fragments.TextDialogFragment;
import threads.server.model.EventViewModel;
import threads.server.model.LiteViewModel;
import threads.server.services.LiteService;
import threads.server.services.MimeTypeService;
import threads.server.utils.CodecDecider;
import threads.server.utils.PermissionAction;
import threads.server.utils.SearchesAdapter;
import threads.server.work.BrowserResetWorker;
import threads.server.work.DownloadContentWorker;
import threads.server.work.DownloadFileWorker;
import threads.server.work.UploadFilesWorker;
import threads.server.work.UploadFolderWorker;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private final ActivityResultLauncher<Intent> mFileForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    try {
                        Objects.requireNonNull(data);

                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);
                        if (!DOCS.hasWritePermission(getApplicationContext(), uri)) {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.file_has_no_write_permission));
                            return;
                        }
                        LiteService.FileInfo fileInfo = LiteService.getFileInfo(getApplicationContext());
                        Objects.requireNonNull(fileInfo);
                        DownloadFileWorker.download(getApplicationContext(), uri, fileInfo.getUri(),
                                fileInfo.getFilename(), fileInfo.getMimeType(), fileInfo.getSize());


                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            });
    private final ActivityResultLauncher<Intent> mContentForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    try {
                        Objects.requireNonNull(data);
                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);
                        if (!DOCS.hasWritePermission(getApplicationContext(), uri)) {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.file_has_no_write_permission));
                            return;
                        }
                        Uri contentUri = LiteService.getContentUri(getApplicationContext());
                        Objects.requireNonNull(contentUri);
                        DownloadContentWorker.download(getApplicationContext(), uri, contentUri);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            });

    private long lastClickTime = 0;
    private SlidingPaneLayout slidingPaneLayout;
    private CoordinatorLayout coordinatorLayout;
    private LiteViewModel liteViewModel;
    private final ActivityResultLauncher<ScanOptions>
            mScanRequestForResult = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() != null) {
                    try {
                        Uri uri = Uri.parse(result.getContents());
                        if (uri != null) {
                            String scheme = uri.getScheme();
                            if (Objects.equals(scheme, Content.IPNS) ||
                                    Objects.equals(scheme, Content.IPFS) ||
                                    Objects.equals(scheme, Content.HTTP) ||
                                    Objects.equals(scheme, Content.HTTPS)) {

                                try {
                                    DOCS docs = DOCS.getInstance(getApplicationContext());
                                    docs.cleanupResolver(uri);
                                } catch (Throwable throwable) {
                                    LogUtils.error(TAG, throwable);
                                }

                                liteViewModel.setUri(uri);
                            } else {
                                EVENTS.getInstance(getApplicationContext()).error(
                                        getString(R.string.codec_not_supported));
                            }
                        } else {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.codec_not_supported));
                        }
                    } catch (Throwable throwable) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.codec_not_supported));
                    }
                }
            });
    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    invokeScan();
                } else {
                    EVENTS.getInstance(getApplicationContext()).permission(
                            getString(R.string.permission_camera_denied));
                }
            });

    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (DOCS.isPartial(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(getApplicationContext(),
                                                    getParentFile(), uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (DOCS.isPartial(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(getApplicationContext(),
                                                getParentFile(), uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private MaterialTextView browserText;
    private ActionMode actionMode;
    private BrowserFragment browserFragment;
    private FilesFragment filesFragment;
    private boolean hasCamera;

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntents(intent);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof BrowserFragment) {
            if (browserFragment.isResumed()) {
                boolean result = browserFragment.onBackPressed();
                if (result) {
                    return;
                }
            }
        }
        if (fragment instanceof FilesFragment) {
            if (filesFragment.isResumed()) {
                boolean result = filesFragment.onBackPressed();
                if (result) {
                    return;
                }
            }
        }
        super.onBackPressed();
    }

    public Fragment getCurrentFragment() {
        if (slidingPaneLayout.isOpen()) {
            return browserFragment;
        }
        return filesFragment;
    }

    public AccessFragment getCurrentAccessFragment() {
        return (AccessFragment) getCurrentFragment();
    }

    private boolean handleIntents(Intent intent) {

        final String action = intent.getAction();

        ShareCompat.IntentReader intentReader = new ShareCompat.IntentReader(this);
        if (Intent.ACTION_SEND.equals(action) ||
                Intent.ACTION_SEND_MULTIPLE.equals(action)) {
            return handleSend(intentReader);
        } else if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {

                try {
                    DOCS docs = DOCS.getInstance(getApplicationContext());
                    docs.cleanupResolver(uri);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                liteViewModel.setUri(uri);
                return true;
            }
        }
        return false;
    }

    private boolean handleSend(ShareCompat.IntentReader intentReader) {

        try {
            Objects.requireNonNull(intentReader);
            if (intentReader.isMultipleShare()) {
                int items = intentReader.getStreamCount();

                if (items > 0) {

                    File file = DOCS.createTempFile(getApplicationContext());

                    try (PrintStream out = new PrintStream(file)) {
                        for (int i = 0; i < items; i++) {
                            Uri uri = intentReader.getStream(i);
                            if (uri != null) {
                                out.println(uri);
                            }
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    UploadFilesWorker.load(getApplicationContext(), 0L, file);
                }
            } else {
                String type = intentReader.getType();
                if (Objects.equals(type, MimeTypeService.PLAIN_MIME_TYPE)) {
                    CharSequence textObject = intentReader.getText();
                    Objects.requireNonNull(textObject);
                    String text = textObject.toString();
                    if (!text.isEmpty()) {

                        Uri uri = Uri.parse(text);
                        if (uri != null) {
                            if (Objects.equals(uri.getScheme(), Content.IPFS) ||
                                    Objects.equals(uri.getScheme(), Content.IPNS) ||
                                    Objects.equals(uri.getScheme(), Content.HTTP) ||
                                    Objects.equals(uri.getScheme(), Content.HTTPS)) {

                                try {
                                    DOCS docs = DOCS.getInstance(getApplicationContext());
                                    docs.cleanupResolver(uri);
                                } catch (Throwable throwable) {
                                    LogUtils.error(TAG, throwable);
                                }


                                liteViewModel.setUri(uri);
                                return true;
                            }
                        }

                        CodecDecider result = CodecDecider.evaluate(text);

                        if (result.getCodex() == CodecDecider.Codec.MULTIHASH) {
                            // it is assumed a CID
                            liteViewModel.setUri(Uri.parse(Content.IPFS + "://" + result.getMultihash()));
                            return true;
                        } else {
                            if (URLUtil.isValidUrl(text)) {
                                liteViewModel.setUri(Uri.parse(text));
                                return true;
                            } else {
                                storeText(text);
                            }
                        }
                    }
                } else if (Objects.equals(type, MimeTypeService.HTML_MIME_TYPE)) {
                    String html = intentReader.getHtmlText();
                    Objects.requireNonNull(html);
                    if (!html.isEmpty()) {
                        storeText(html);
                    }
                } else {
                    Uri uri = intentReader.getStream();
                    Objects.requireNonNull(uri);

                    if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.file_has_no_read_permission));
                        return false;
                    }

                    if (DOCS.isPartial(getApplicationContext(), uri)) {

                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.file_not_found));

                        return false;
                    }

                    LiteService.file(getApplicationContext(), 0L, uri);

                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    private void storeText(@NonNull String text) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                DOCS docs = DOCS.getInstance(getApplicationContext());
                docs.createTextFile(0L, text);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }


    private ActionMode.Callback createSearchActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_searchable, menu);
                mode.setTitle("");

                MenuItem scanMenuItem = menu.findItem(R.id.action_scan);
                if (!hasCamera) {
                    scanMenuItem.setVisible(false);
                }
                MenuItem searchMenuItem = menu.findItem(R.id.action_search);
                SearchView mSearchView = (SearchView) searchMenuItem.getActionView();

                TextView textView = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_src_text);
                textView.setTextSize(16);
                textView.setMinimumHeight(actionBarSize());

                ImageView magImage = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_mag_icon);
                magImage.setVisibility(View.GONE);
                magImage.setImageDrawable(null);

                mSearchView.setIconifiedByDefault(false);
                mSearchView.setIconified(false);
                mSearchView.setSubmitButtonEnabled(false);
                mSearchView.setQueryHint(getString(R.string.enter_url));
                mSearchView.setFocusable(true);
                mSearchView.requestFocus();


                ListPopupWindow mPopupWindow = new ListPopupWindow(MainActivity.this,
                        null, android.R.attr.contextPopupMenuStyle) {

                    @Override
                    public boolean isInputMethodNotNeeded() {
                        return true;
                    }
                };
                mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
                mPopupWindow.setModal(false);
                mPopupWindow.setAnchorView(mSearchView);
                mPopupWindow.setVerticalOffset(actionBarSize());
                mPopupWindow.setAnimationStyle(android.R.style.Animation);


                mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        try {
                            mPopupWindow.dismiss();

                            if (actionMode != null) {
                                actionMode.finish();
                            }
                            if (query != null && !query.isEmpty()) {
                                Uri uri = Uri.parse(query);
                                String scheme = uri.getScheme();
                                if (Objects.equals(scheme, Content.IPNS) ||
                                        Objects.equals(scheme, Content.IPFS) ||
                                        Objects.equals(scheme, Content.HTTP) ||
                                        Objects.equals(scheme, Content.HTTPS)) {
                                    liteViewModel.setUri(uri);
                                } else {

                                    try {
                                        liteViewModel.setUri(Uri.parse(Content.IPFS + "://" +
                                                Cid.decode(query)));
                                    } catch (Throwable ignore) {
                                        liteViewModel.setUri(Uri.parse(
                                                Settings.getDefaultSearchEngine(query)));
                                    }
                                }
                            }
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        if (!newText.isEmpty()) {
                            BOOKS books = BOOKS.getInstance(getApplicationContext());
                            List<Bookmark> bookmarks = books.getBookmarksByQuery(newText);

                            if (!bookmarks.isEmpty()) {
                                mPopupWindow.setAdapter(new SearchesAdapter(
                                        MainActivity.this, new ArrayList<>(bookmarks)) {
                                    @Override
                                    public void onClick(@NonNull Bookmark bookmark) {
                                        try {
                                            java.lang.Thread.sleep(150);
                                            liteViewModel.setUri(Uri.parse(bookmark.getUri()));
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        } finally {
                                            mPopupWindow.dismiss();
                                            releaseActionMode();
                                        }
                                    }
                                });
                                mPopupWindow.show();
                                return true;
                            } else {
                                mPopupWindow.dismiss();
                            }
                        } else {
                            mPopupWindow.dismiss();
                        }

                        return false;
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.action_scan) {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return false;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();


                        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                            return false;
                        }

                        invokeScan();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        mode.finish();
                    }
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
            }
        };

    }

    private int actionBarSize() {
        TypedValue tv = new TypedValue();
        getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true);
        return getResources().getDimensionPixelSize(tv.resourceId);
    }

    private void showMultiaddrs() throws Exception {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(
                MainActivity.this);
        builder.setTitle(R.string.multiaddrs);

        String link = "";
        DOCS docs = DOCS.getInstance(getApplicationContext());
        for (Multiaddr multiaddr : docs.dialableAddresses()) {
            link = link.concat("\n").concat(multiaddr.toString()).concat("\n");
        }
        builder.setMessage(link);

        builder.setPositiveButton(getString(android.R.string.ok),
                (dialogInterface, which) -> dialogInterface.cancel());
        String finalLink = link;
        builder.setNeutralButton(getString(android.R.string.copy),
                (dialogInterface, which) -> {
                    LogUtils.error(TAG, finalLink);
                    ClipboardManager clipboardManager = (ClipboardManager)
                            getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(
                            getString(R.string.multiaddrs), finalLink);
                    clipboardManager.setPrimaryClip(clipData);
                });
        builder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        browserFragment = (BrowserFragment)
                getSupportFragmentManager().findFragmentById(R.id.browser_container);
        filesFragment = (FilesFragment) getSupportFragmentManager()
                .findFragmentById(R.id.files_container);

        slidingPaneLayout = findViewById(R.id.sliding_pane_layout);
        getOnBackPressedDispatcher().addCallback(
                this,
                new TwoPaneOnBackPressedCallback(slidingPaneLayout));


        PackageManager pm = getPackageManager();
        hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
        coordinatorLayout = findViewById(R.id.drawer_layout);

        MaterialTextView offline_mode = findViewById(R.id.offline_mode);
        AppBarLayout mAppBar = findViewById(R.id.appbar);


        mAppBar.addOnOffsetChangedListener(new AppBarStateChangedListener() {
            @Override
            public void onStateChanged(State state) {
                if (state == State.EXPANDED) {
                    browserFragment.enableSwipeRefresh(true);
                } else if (state == State.COLLAPSED) {
                    browserFragment.enableSwipeRefresh(false);
                }
            }
        });

        MaterialToolbar materialToolbar = findViewById(R.id.toolbar);
        materialToolbar.setNavigationOnClickListener(v -> showFilesFragment());


        Button mActionBookmarks = findViewById(R.id.action_bookmarks);
        mActionBookmarks.setOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                BookmarksDialogFragment dialogFragment = new BookmarksDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), BookmarksDialogFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        Button mActionOverflow = findViewById(R.id.action_overflow);
        mActionOverflow.setOnClickListener(v -> {

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                    return;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);


                View menuOverflow = inflater.inflate(
                        R.layout.menu_overflow, coordinatorLayout, false);

                PopupWindow dialog = new PopupWindow(
                        MainActivity.this, null, android.R.attr.contextPopupMenuStyle);
                dialog.setContentView(menuOverflow);
                dialog.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setOutsideTouchable(true);
                dialog.setFocusable(true);
                dialog.showAsDropDown(mActionOverflow, 0, -actionBarSize());


                Button actionNextPage = menuOverflow.findViewById(R.id.action_next_page);
                actionNextPage.setEnabled(browserFragment.canGoForward());
                actionNextPage.setOnClickListener(v1 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        browserFragment.goForward();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                MaterialButton actionBookmark = menuOverflow.findViewById(R.id.action_bookmark);

                BOOKS books = BOOKS.getInstance(getApplicationContext());
                Uri uri = liteViewModel.getUri().getValue();
                boolean isBrowserFragment = slidingPaneLayout.isOpen();

                if (uri != null) {
                    if (books.hasBookmark(uri.toString())) {
                        actionBookmark.setIcon(AppCompatResources.getDrawable(
                                getApplicationContext(), R.drawable.star));
                    } else {
                        actionBookmark.setIcon(AppCompatResources.getDrawable(
                                getApplicationContext(), R.drawable.star_outline));
                    }
                } else {
                    actionBookmark.setEnabled(false);
                }

                actionBookmark.setOnClickListener(v1 -> {

                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        browserFragment.bookmark(getApplicationContext());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                Button actionFindPage = menuOverflow.findViewById(R.id.action_find_page);
                actionFindPage.setEnabled(true);
                actionFindPage.setOnClickListener(v12 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        getCurrentAccessFragment().findInPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionDownload = menuOverflow.findViewById(R.id.action_download);

                actionDownload.setEnabled(downloadActive());

                actionDownload.setOnClickListener(v13 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        Objects.requireNonNull(uri);
                        contentDownloader(uri);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionShare = menuOverflow.findViewById(R.id.action_share);
                actionShare.setEnabled(true);
                actionShare.setOnClickListener(v14 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        DOCS docs = DOCS.getInstance(getApplicationContext());
                        String link = docs.getHomePageUri().toString();
                        if (isBrowserFragment) {
                            if (uri != null) {
                                link = uri.toString();
                            }
                        }

                        ComponentName[] names = {new ComponentName(getApplicationContext(),
                                MainActivity.class)};

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
                        intent.putExtra(Intent.EXTRA_TEXT, link);
                        intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                        intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                        Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                        chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
                        startActivity(chooser);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionReload = menuOverflow.findViewById(R.id.action_reload);

                actionReload.setEnabled(isBrowserFragment);
                actionReload.setOnClickListener(v15 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        browserFragment.reload();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionClearData = menuOverflow.findViewById(R.id.action_cleanup);

                actionClearData.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(MainActivity.this);
                        alertDialog.setTitle(getString(R.string.warning));
                        alertDialog.setMessage(getString(R.string.clear_browser_data));
                        alertDialog.setPositiveButton(getString(android.R.string.ok),
                                (dialogInterface, which) -> {
                                    browserFragment.clearBrowserData();
                                    BrowserResetWorker.reset(getApplicationContext());
                                    liteViewModel.setParentFileIdx(0L);
                                    dialog.dismiss();
                                });
                        alertDialog.setNeutralButton(getString(android.R.string.cancel),
                                (dialogInterface, which) -> dialog.dismiss());
                        alertDialog.show();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionInformation = menuOverflow.findViewById(R.id.action_information);

                actionInformation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        if (isBrowserFragment) {
                            Objects.requireNonNull(uri);
                            ContentDialogFragment.newInstance(getString(R.string.information),
                                            getString(R.string.url_access),
                                            uri, false)
                                    .show(getSupportFragmentManager(), ContentDialogFragment.TAG);
                        } else {
                            DOCS docs = DOCS.getInstance(getApplicationContext());
                            ContentDialogFragment.newInstance(getString(R.string.homepage),
                                            getString(R.string.connection_text),
                                            docs.getHomePageUri(), true)
                                    .show(getSupportFragmentManager(), ContentDialogFragment.TAG);
                        }

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionNewFolder = menuOverflow.findViewById(R.id.action_new_folder);
                if (!isBrowserFragment) {
                    actionNewFolder.setVisibility(View.VISIBLE);
                } else {
                    actionNewFolder.setVisibility(View.GONE);
                }
                actionNewFolder.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);


                        NewFolderDialogFragment.newInstance(
                                        liteViewModel.getParentFileIdxValue()).
                                show(getSupportFragmentManager(), NewFolderDialogFragment.TAG);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionImportFolder = menuOverflow.findViewById(R.id.action_import_folder);
                if (!isBrowserFragment) {
                    actionImportFolder.setVisibility(View.VISIBLE);
                } else {
                    actionImportFolder.setVisibility(View.GONE);
                }
                actionImportFolder.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                        mFolderImportForResult.launch(intent);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionNewText = menuOverflow.findViewById(R.id.action_new_text);
                if (!isBrowserFragment) {
                    actionNewText.setVisibility(View.VISIBLE);
                } else {
                    actionNewText.setVisibility(View.GONE);
                }
                actionNewText.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        TextDialogFragment.newInstance(liteViewModel.getParentFileIdxValue()).
                                show(getSupportFragmentManager(), TextDialogFragment.TAG);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionMultiaddrs = menuOverflow.findViewById(R.id.action_multiaddrs);
                if (!isBrowserFragment) {
                    actionMultiaddrs.setVisibility(View.VISIBLE);
                } else {
                    actionMultiaddrs.setVisibility(View.GONE);
                }
                actionMultiaddrs.setOnClickListener(v20 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        showMultiaddrs();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                MaterialDivider divider = menuOverflow.findViewById(R.id.divider);
                if (!isBrowserFragment) {
                    divider.setVisibility(View.VISIBLE);
                } else {
                    divider.setVisibility(View.GONE);
                }

                TextView actionSettings = menuOverflow.findViewById(R.id.action_settings);

                actionSettings.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        SettingsDialogFragment dialogFragment = new SettingsDialogFragment();
                        dialogFragment.show(getSupportFragmentManager(), SettingsDialogFragment.TAG);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionDocumentation = menuOverflow.findViewById(R.id.action_documentation);

                actionDocumentation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        lastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        liteViewModel.setUri(
                                Uri.parse("https://gitlab.com/remmer.wilts/ipfs-lite"));
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        browserText = findViewById(R.id.action_browser);

        browserText.setOnClickListener(view -> {
            try {
                if (!slidingPaneLayout.isOpen()) {
                    slidingPaneLayout.open();
                } else {
                    actionMode = startSupportActionMode(
                            createSearchActionModeCallback());
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        liteViewModel = new ViewModelProvider(this).get(LiteViewModel.class);


        liteViewModel.getParentFileIdx().observe(this, (parent) -> {
            if (parent != null) {
                slidingPaneLayout.closePane();
            }
        });

        liteViewModel.getUri().observe(this, (uri) -> {
            if (uri != null) {
                slidingPaneLayout.open();
            }
        });


        EventViewModel eventViewModel =
                new ViewModelProvider(this).get(EventViewModel.class);


        eventViewModel.delete().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {

                        String data = content
                                .replace("[", "")
                                .replace("]", "")
                                .trim();

                        String[] parts = data.split(",");

                        long[] idxs = new long[parts.length];
                        for (int i = 0; i < parts.length; i++) {
                            idxs[i] = Long.parseLong(parts[i].trim());
                        }


                        String message;
                        if (idxs.length == 1) {
                            message = getString(R.string.delete_file);
                        } else {
                            message = getString(
                                    R.string.delete_files, "" + idxs.length);
                        }
                        AtomicBoolean deleteThreads = new AtomicBoolean(true);
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);

                        snackbar.setAction(getString(R.string.revert_operation), (view) -> {

                            try {
                                deleteThreads.set(false);
                                FILES files = FILES.getInstance(getApplicationContext());
                                Executors.newSingleThreadExecutor().execute(() ->
                                        files.resetDeleting(idxs));
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            } finally {
                                snackbar.dismiss();
                            }

                        });

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (deleteThreads.get()) {
                                    ExecutorService executor = Executors.newSingleThreadExecutor();
                                    executor.execute(() -> {
                                        try {
                                            DOCS.getInstance(getApplicationContext())
                                                    .deleteDocuments(idxs);
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        }
                                    });

                                }
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }

                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.error().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.connections().observe(this, (event) -> {
            try {
                if (event != null) {
                    DOCS docs = DOCS.getInstance(getApplicationContext());
                    int connections = docs.getServer().numServerConnections();
                    LogUtils.error(TAG, "Number of server connections " + connections);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.reachability().observe(this, (event) -> {
            try {
                if (event != null) {
                    String reachability = event.getContent();
                    String text = "";
                    if (reachability.equals(DOCS.Reachability.LOCAL.name())) {
                        text = getString(R.string.service_local_reachable);
                    } else if (reachability.equals(DOCS.Reachability.GLOBAL.name())) {
                        text = getString(R.string.service_reachable);
                    } else if (reachability.equals(DOCS.Reachability.RELAYS.name())) {
                        text = getString(R.string.service_relays_reachable);
                    }

                    if (!text.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, text,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.fatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.permission().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.app_settings, new PermissionAction());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();

                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.warning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.info().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.toolbar().observe(this, (event) -> {
            try {
                if (event != null) {
                    Uri uri = Uri.parse(event.getContent());
                    eventViewModel.removeEvent(event);


                    if (Objects.equals(uri.getScheme(), Content.HTTPS)) {
                        browserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                R.drawable.lock, 0, 0, 0
                        );
                        browserText.setText(prettyUri(uri, "https://"));
                    } else if (Objects.equals(uri.getScheme(), Content.HTTP)) {
                        browserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                R.drawable.lock_open, 0, 0, 0
                        );
                        browserText.setText(prettyUri(uri, "http://"));
                    } else {

                        browserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                R.drawable.lock, 0, 0, 0
                        );

                        BOOKS books = BOOKS.getInstance(getApplicationContext());
                        Bookmark bookmark = books.getBookmark(uri.toString());

                        String title = uri.toString();
                        if (bookmark != null) {
                            String bookmarkTitle = bookmark.getTitle();
                            if (!bookmarkTitle.isEmpty()) {
                                title = bookmarkTitle;
                            }
                        }

                        browserText.setText(title);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.online().observe(this, (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.GONE);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });
        eventViewModel.offline().observe(this, (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.VISIBLE);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        try {
            DOCS.getInstance(getApplicationContext()).darkMode.set(isDarkTheme());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        Intent intent = getIntent();
        boolean handled = handleIntents(intent);
        if (!handled) {
            // show files view as primary fragment
            liteViewModel.setParentFileIdx(0);
        }

    }

    private void showFilesFragment() {
        try {
            if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            liteViewModel.setParentFileIdx(0L);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private String prettyUri(@NonNull Uri uri, @NonNull String replace) {
        return uri.toString().replaceFirst(replace, "");
    }

    private boolean downloadActive() {
        Uri uri = liteViewModel.getUri().getValue();
        if (uri != null) {
            return Objects.equals(uri.getScheme(), Content.IPFS) ||
                    Objects.equals(uri.getScheme(), Content.IPNS);
        }
        return false;
    }

    private void releaseActionMode() {
        try {
            if (actionMode != null) {
                actionMode.finish();
                actionMode = null;
            }
            getCurrentAccessFragment().releaseActionMode();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void invokeScan() {
        try {
            PackageManager pm = getPackageManager();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                ScanOptions options = new ScanOptions();
                options.setDesiredBarcodeFormats(ScanOptions.ALL_CODE_TYPES);
                options.setPrompt(getString(R.string.scan_url));
                options.setCameraId(0);  // Use a specific camera of the device
                options.setBeepEnabled(true);
                options.setOrientationLocked(false);
                mScanRequestForResult.launch(options);
            } else {
                EVENTS.getInstance(getApplicationContext()).permission(
                        getString(R.string.feature_camera_required));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private boolean isDarkTheme() {
        int nightModeFlags = getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK;
        return nightModeFlags == Configuration.UI_MODE_NIGHT_YES;
    }

    private void tearDown(@NonNull PopupWindow dialog) {
        try {
            java.lang.Thread.sleep(150);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            dialog.dismiss();
        }
    }


    public void fileDownloader(@NonNull Uri uri, @NonNull String filename,
                               @NonNull String mimeType, long size) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.download_title);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {


            LiteService.setFileInfo(getApplicationContext(), uri, filename, mimeType, size);

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI,
                    Uri.parse(Settings.DOWNLOADS));
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            mFileForResult.launch(intent);

        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> dialog.cancel());
        builder.show();

    }

    public void contentDownloader(@NonNull Uri uri) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.download_title);
        String filename = DOCS.getFileName(uri);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            LiteService.setContentUri(getApplicationContext(), uri);

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI,
                    Uri.parse(Settings.DOWNLOADS));
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            mContentForResult.launch(intent);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> dialog.cancel());
        builder.show();

    }


    private long getParentFile() {
        return liteViewModel.getParentFileIdxValue();
    }

    public abstract static class AppBarStateChangedListener implements AppBarLayout.OnOffsetChangedListener {

        private State mCurrentState = State.IDLE;

        @Override
        public final void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            if (verticalOffset == 0) {
                setCurrentStateAndNotify(State.EXPANDED);
            } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                setCurrentStateAndNotify(State.COLLAPSED);
            } else {
                setCurrentStateAndNotify(State.IDLE);
            }
        }

        private void setCurrentStateAndNotify(State state) {
            if (mCurrentState != state) {
                onStateChanged(state);
            }
            mCurrentState = state;
        }

        public abstract void onStateChanged(State state);

        public enum State {
            EXPANDED,
            COLLAPSED,
            IDLE
        }
    }

    class TwoPaneOnBackPressedCallback extends OnBackPressedCallback
            implements SlidingPaneLayout.PanelSlideListener {

        private final SlidingPaneLayout mSlidingPaneLayout;

        TwoPaneOnBackPressedCallback(@NonNull SlidingPaneLayout slidingPaneLayout) {
            // Set the default 'enabled' state to true only if it is slide able (i.e., the panes
            // are overlapping) and open (i.e., the detail pane is visible).
            super(slidingPaneLayout.isSlideable() && slidingPaneLayout.isOpen());
            mSlidingPaneLayout = slidingPaneLayout;
            slidingPaneLayout.addPanelSlideListener(this);
        }

        @Override
        public void handleOnBackPressed() {
            // Return to the list pane when the system back button is pressed.
            mSlidingPaneLayout.closePane();
        }

        @Override
        public void onPanelSlide(@NonNull View panel, float slideOffset) {
        }

        @Override
        public void onPanelOpened(@NonNull View panel) {
            // Intercept the system back button when the detail pane becomes visible.
            setEnabled(true);

            invalidateOptionsMenu();
        }

        @Override
        public void onPanelClosed(@NonNull View panel) {
            // Disable intercepting the system back button when the user returns to the
            // list pane.
            setEnabled(false);

            invalidateOptionsMenu();
        }
    }
}