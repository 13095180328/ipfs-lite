package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.core.Progress;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;


public class DownloadFileWorker extends Worker {

    private static final String TAG = DownloadFileWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public DownloadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, @NonNull Uri source,
                                              @NonNull String filename, @NonNull String mimeType,
                                              long size) {
        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putString(Content.NAME, filename);
        data.putString(Content.TYPE, mimeType);
        data.putLong(Content.SIZE, size);
        data.putString(Content.FILE, source.toString());

        return new OneTimeWorkRequest.Builder(DownloadFileWorker.class)
                .setInputData(data.build())
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri, @NonNull Uri source,
                                @NonNull String filename, @NonNull String mimeType, long size) {
        WorkManager.getInstance(context).enqueue(getWork(uri, source, filename, mimeType, size));
    }


    @NonNull
    @Override
    public Result doWork() {

        String dest = getInputData().getString(Content.URI);
        Objects.requireNonNull(dest);
        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + dest);

        try {
            Uri uriDest = Uri.parse(dest);
            DocumentFile doc = DocumentFile.fromTreeUri(getApplicationContext(), uriDest);
            Objects.requireNonNull(doc);


            long size = getInputData().getLong(Content.SIZE, 0);
            String name = getInputData().getString(Content.NAME);
            Objects.requireNonNull(name);
            String mimeType = getInputData().getString(Content.TYPE);
            Objects.requireNonNull(mimeType);

            String url = getInputData().getString(Content.FILE);
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);


            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                    InitApplication.STORAGE_CHANNEL_ID);


            PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent main = new Intent(getApplicationContext(), MainActivity.class);

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    intent).build();

            builder.setContentText(name)
                    .setSubText("" + 0 + "%")
                    .setContentIntent(pendingIntent)
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setUsesChronometer(true)
                    .setOngoing(true);


            Notification notification = builder.build();

            int notificationId = getId().hashCode();

            notificationManager.notify(notificationId, notification);
            setForegroundAsync(new ForegroundInfo(notificationId, notification));

            HttpURLConnection.setFollowRedirects(false);

            URL urlCon = new URL(uri.toString());
            HttpURLConnection huc = (HttpURLConnection) urlCon.openConnection();

            huc.setReadTimeout(30000);
            huc.connect();

            DocumentFile child = doc.createFile(mimeType, name);
            Objects.requireNonNull(child);
            try (InputStream is = huc.getInputStream()) {
                try (OutputStream os = getApplicationContext().
                        getContentResolver().openOutputStream(child.getUri())) {
                    Objects.requireNonNull(os);
                    IPFS.copy(is, os, new Progress() {
                        @Override
                        public void setProgress(int progress) {
                            builder.setSubText("" + progress + "%")
                                    .setProgress(100, progress, false);
                            notificationManager.notify(notificationId, builder.build());
                        }

                        @Override
                        public boolean isCancelled() {
                            return isStopped();
                        }

                    }, size);
                }
            } catch (Throwable e) {
                child.delete();
                if (!isStopped()) {
                    builder.setContentText(getApplicationContext()
                                    .getString(R.string.download_canceled, name))
                            .setSubText("")
                            .setProgress(0, 0, false);
                    notificationManager.notify(notificationId, builder.build());
                }
                throw e;
            }

            builder.setContentText(getApplicationContext().
                            getString(R.string.download_complete, name))
                    .setSubText("")
                    .setProgress(0, 0, false);
            notificationManager.notify(notificationId, builder.build());

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }

}
