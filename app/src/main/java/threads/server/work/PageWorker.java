package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.DOCS;

public class PageWorker extends Worker {
    private static final String TAG = PageWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public PageWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(int delay) {

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        return new OneTimeWorkRequest.Builder(PageWorker.class)
                .addTag(TAG)
                .setConstraints(constraints)
                .setInitialDelay(delay, TimeUnit.SECONDS)
                .build();

    }

    public static void publish(@NonNull Context context, int delay) {
        WorkManager.getInstance(context).
                enqueueUniqueWork(TAG, ExistingWorkPolicy.REPLACE, getWork(delay));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, "Start ...");

        try {

            DOCS docs = DOCS.getInstance(getApplicationContext());

            if (docs.globalPublishingEnabled()) {
                // only when the node is global reachable, publishing should be possible

                NotificationManager notificationManager = (NotificationManager)
                        getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                        InitApplication.PUBLISH_CHANNEL_ID);


                PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                        .createCancelPendingIntent(getId());
                String cancel = getApplicationContext().getString(android.R.string.cancel);

                Intent main = new Intent(getApplicationContext(), MainActivity.class);

                int requestID = (int) System.currentTimeMillis();
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                        main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                Notification.Action action = new Notification.Action.Builder(
                        Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                        intent).build();

                builder.setContentText(getApplicationContext().getString(R.string.publish_homepage))
                        .setContentIntent(pendingIntent)
                        .setOnlyAlertOnce(true)
                        .setSmallIcon(R.drawable.baseline_restore_24)
                        .addAction(action)
                        .setCategory(Notification.CATEGORY_SERVICE)
                        .setUsesChronometer(true)
                        .setOngoing(true);


                Notification notification = builder.build();

                int notificationId = getId().hashCode();

                notificationManager.notify(notificationId, notification);
                setForegroundAsync(new ForegroundInfo(notificationId, notification));

                publishPage();

                WorkManager.getInstance(getApplicationContext()).
                        enqueueUniqueWork(TAG, ExistingWorkPolicy.APPEND,
                                getWork(60 * 60 * 24)); // next in 24 hours
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "Finish  onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


    public void publishPage() throws Exception {

        DOCS docs = DOCS.getInstance(getApplicationContext());
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        LogUtils.error(TAG, docs.getHomePageUri().toString());
        Server server = docs.getServer();
        try (Session session = ipfs.createSession(false)) {

            int maxProviders = 100;
            int timeout = 60 * 5; // 5 min

            IpnsEntity page = docs.getHomePage();
            if (page != null) {

                long seq = page.getSequence();
                Cid cid = IpnsEntity.decodeIpnsData(page.getValue());
                Objects.requireNonNull(cid);
                LogUtils.error(TAG, "ipfs://" + cid);

                try {
                    Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();
                    // publish ipns entry
                    ipfs.publishName(session, seq, cid, providers::add, new TimeoutCancellable(
                            () -> isStopped() || providers.size() > maxProviders,
                            timeout));

                    LogUtils.error(TAG, "Published ipns to " + providers.size());
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                try {
                    ipfs.provideRecursively(server, cid, this::isStopped);
                    LogUtils.error(TAG, "Published recursively");
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        }
    }

}

