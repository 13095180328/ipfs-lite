package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.List;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.files.FILES;
import threads.server.core.files.FileInfo;

public class CopyDirectoryWorker extends Worker {
    private static final String WID = "UDW";
    private static final String TAG = CopyDirectoryWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public CopyDirectoryWorker(@NonNull Context context,
                               @NonNull WorkerParameters params) {
        super(context, params);

    }


    private static OneTimeWorkRequest getWork(@NonNull Uri uri, long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);
        data.putString(Content.URI, uri.toString());

        return new OneTimeWorkRequest.Builder(CopyDirectoryWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void copyTo(@NonNull Context context, @NonNull Uri uri, long idx) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + idx, ExistingWorkPolicy.KEEP, getWork(uri, idx));

    }


    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);
        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        try {

            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());

            int notificationId = getId().hashCode();

            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            try (Session session = ipfs.createSession()) {
                FileInfo fileInfo = files.getFileInfo(idx);
                Objects.requireNonNull(fileInfo);

                DocumentFile rootDocFile = DocumentFile.fromTreeUri(getApplicationContext(),
                        Uri.parse(uri));
                Objects.requireNonNull(rootDocFile);

                DocumentFile docFile = rootDocFile.createDirectory(fileInfo.getName());
                Objects.requireNonNull(docFile);

                Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                        InitApplication.STORAGE_CHANNEL_ID);


                PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                        .createCancelPendingIntent(getId());
                String cancel = getApplicationContext().getString(android.R.string.cancel);

                Intent main = new Intent(getApplicationContext(), MainActivity.class);

                int requestID = (int) System.currentTimeMillis();
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                        main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);


                Notification.Action action = new Notification.Action.Builder(
                        Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                        intent).build();


                builder.setContentTitle(fileInfo.getName())
                        .setSubText("" + 0 + "%")
                        .setContentIntent(pendingIntent)
                        .setProgress(100, 0, false)
                        .setOnlyAlertOnce(true)
                        .setSmallIcon(R.drawable.download)
                        .addAction(action)
                        .setCategory(Notification.CATEGORY_PROGRESS)
                        .setOngoing(true)
                        .setUsesChronometer(true);

                Notification notification = builder.build();

                notificationManager.notify(notificationId, notification);
                setForegroundAsync(new ForegroundInfo(notificationId, notification));

                copyThreads(session, builder, fileInfo, docFile, notificationId);
            } finally {
                notificationManager.cancel(notificationId);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private void copyThreads(@NonNull Session session, @NonNull Notification.Builder builder,
                             @NonNull FileInfo fileInfo, @NonNull DocumentFile file, int notificationId) {
        FILES files = FILES.getInstance(getApplicationContext());

        List<FileInfo> children = files.getChildren(fileInfo.getIdx());
        for (FileInfo child : children) {
            if (!isStopped()) {
                if (!child.isDeleting() && child.isSeeding()) {
                    if (child.isDir()) {
                        DocumentFile docFile = file.createDirectory(child.getName());
                        Objects.requireNonNull(docFile);
                        copyThreads(session, builder, child, docFile, notificationId);
                    } else {
                        DocumentFile childFile = file.createFile(
                                child.getMimeType(), child.getName());
                        Objects.requireNonNull(childFile);
                        if (!childFile.canWrite()) {
                            // throw message
                            LogUtils.error(TAG, "can not write");
                        } else {
                            copyThread(session, builder, child, childFile, notificationId);
                        }
                    }
                }
            }
        }
    }

    private void copyThread(@NonNull Session session, @NonNull Notification.Builder builder,
                            @NonNull FileInfo fileInfo, @NonNull DocumentFile file, int notificationId) {

        if (isStopped()) {
            return;
        }
        NotificationManager notificationManager = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Cid cid = fileInfo.getCid();
        Objects.requireNonNull(cid);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            try (OutputStream os = getApplicationContext().getContentResolver().
                    openOutputStream(file.getUri())) {
                Objects.requireNonNull(os);
                ipfs.fetchToOutputStream(session, os, cid, new Progress() {

                    @Override
                    public void setProgress(int progress) {
                        builder.setSubText("" + progress + "%")
                                .setContentTitle(fileInfo.getName())
                                .setProgress(100, progress, false);
                        notificationManager.notify(notificationId, builder.build());
                    }

                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }


                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            if (isStopped()) {
                if (file.exists()) {
                    file.delete();
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
