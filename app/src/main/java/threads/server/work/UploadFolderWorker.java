package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;


public class UploadFolderWorker extends Worker {
    private static final String TAG = UploadFolderWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFolderWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long idx, @NonNull Uri uri) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFolderWorker.class)
                .addTag(UploadFolderWorker.class.getSimpleName())
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, long idx, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                UploadFolderWorker.class.getSimpleName(),
                ExistingWorkPolicy.APPEND, getWork(idx, uri));

    }


    @NonNull
    @Override
    public Result doWork() {

        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);
        long root = getInputData().getLong(Content.IDX, 0L);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);


        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());
            DocumentFile rootDocFile = DocumentFile.fromTreeUri(getApplicationContext(),
                    Uri.parse(uri));
            Objects.requireNonNull(rootDocFile);

            int notificationId = getId().hashCode();

            String name = rootDocFile.getName();
            Objects.requireNonNull(name);


            Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                    InitApplication.STORAGE_CHANNEL_ID);


            PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent main = new Intent(getApplicationContext(), MainActivity.class);

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    intent).build();

            builder.setContentTitle(name)
                    .setSubText("" + 0 + "/" + 0)
                    .setContentIntent(pendingIntent)
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setOngoing(true);

            Notification notification = builder.build();

            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificationId, notification);
            setForegroundAsync(new ForegroundInfo(notificationId, notification));


            long parent = createDir(root, name);
            try (Session session = ipfs.createSession(false)) {

                files.setWork(parent, getId());
                files.setUri(parent, uri);
                files.setLeaching(parent);

                copyDir(session, builder, parent, rootDocFile, notificationId);

            } finally {
                files.setDone(parent);
                notificationManager.cancel(notificationId);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private long createDir(long parent, @NonNull String name) throws Exception {
        DOCS docs = DOCS.getInstance(getApplicationContext());
        return docs.createDirectory(parent, name);
    }

    private void copyDir(Session session, Notification.Builder builder,
                         long parent, DocumentFile file, int notificationId) throws Exception {
        DOCS docs = DOCS.getInstance(getApplicationContext());
        FILES files = FILES.getInstance(getApplicationContext());
        DocumentFile[] filesInDir = file.listFiles();
        int maxIndex = filesInDir.length;
        int index = 0;
        for (DocumentFile docFile : filesInDir) {

            if (!isStopped()) {
                index++;
                if (docFile.isDirectory()) {
                    String name = docFile.getName();
                    Objects.requireNonNull(name);
                    long child = createDir(parent, name);
                    copyDir(session, builder, child, docFile, notificationId);
                    files.setDone(child);
                    docs.finishDocument(child);
                } else {
                    long child = copyFile(session, builder, parent, docFile,
                            index, maxIndex, notificationId);
                    docs.finishDocument(child);
                }
            }
        }
    }

    private long copyFile(Session session, Notification.Builder builder, long parent,
                          DocumentFile file, int index,
                          int maxIndex, int notificationId) throws Exception {

        if (isStopped()) {
            return 0L;
        }

        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        FILES files = FILES.getInstance(getApplicationContext());
        EVENTS events = EVENTS.getInstance(getApplicationContext());

        NotificationManager notificationManager = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        long idx = createDocument(parent, file);
        long size = file.length();
        String name = file.getName();
        Objects.requireNonNull(name);
        files.setWork(idx, getId());
        Uri uri = file.getUri();

        try (InputStream inputStream = getApplicationContext().getContentResolver().
                openInputStream(uri)) {
            Objects.requireNonNull(inputStream);

            Cid cid = ipfs.storeInputStream(session, inputStream, new Progress() {

                @Override
                public void setProgress(int progress) {
                    builder.setSubText("" + index + "/" + maxIndex)
                            .setContentTitle(name)
                            .setProgress(100, progress, false);
                    notificationManager.notify(notificationId, builder.build());
                }

                @Override
                public boolean isCancelled() {
                    return isStopped();
                }

            }, size);


            files.setDone(idx, cid);
            return idx;

        } catch (Throwable throwable) {
            files.setDeleting(idx);
            events.warning(getApplicationContext().getString(
                    R.string.download_canceled, name));
        }

        return 0L;
    }

    private long createDocument(long parent, @NonNull DocumentFile file) throws Exception {

        Uri uri = file.getUri();
        long size = file.length();
        String name = file.getName();
        Objects.requireNonNull(name);
        String mimeType = file.getType();
        DOCS docs = DOCS.getInstance(getApplicationContext());

        return docs.createDocument(parent, mimeType, null, uri, name,
                size, false, false);
    }

}
