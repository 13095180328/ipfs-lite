package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Link;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.locals.LOCALS;
import threads.server.services.MimeTypeService;

public class DownloadContentWorker extends Worker {

    private static final String TAG = DownloadContentWorker.class.getSimpleName();

    private final AtomicBoolean success = new AtomicBoolean(true);

    @SuppressWarnings("WeakerAccess")
    public DownloadContentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, @NonNull Uri content) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putString(Content.ADDR, content.toString());

        return new OneTimeWorkRequest.Builder(DownloadContentWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri, @NonNull Uri content) {
        WorkManager.getInstance(context).enqueue(getWork(uri, content));
    }

    @NonNull
    @Override
    public Result doWork() {

        String dest = getInputData().getString(Content.URI);
        Objects.requireNonNull(dest);
        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + dest);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            LOCALS locals = LOCALS.getInstance();
            int notificationId = getId().hashCode();
            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);


            try (Session session = ipfs.createSession(true)) {

                locals.getLocals().parallelStream().forEach(multiaddr -> {
                    try {
                        if (!Objects.equals(multiaddr.getPeerId(), ipfs.self())) {
                            LogUtils.error(TAG, "try " + multiaddr);
                            session.connect(multiaddr, ipfs.getConnectionParameters());
                        }
                    } catch (Throwable ignore) {
                    }
                });

                Uri uriDest = Uri.parse(dest);
                DocumentFile doc = DocumentFile.fromTreeUri(getApplicationContext(), uriDest);
                Objects.requireNonNull(doc);


                String url = getInputData().getString(Content.ADDR);
                Objects.requireNonNull(url);
                Uri uri = Uri.parse(url);
                String name = DOCS.getFileName(uri);


                Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                        InitApplication.STORAGE_CHANNEL_ID);

                PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                        .createCancelPendingIntent(getId());
                String cancel = getApplicationContext().getString(android.R.string.cancel);

                Intent main = new Intent(getApplicationContext(), MainActivity.class);

                int requestID = (int) System.currentTimeMillis();
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                        main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                Notification.Action action = new Notification.Action.Builder(
                        Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                        intent).build();

                builder.setContentTitle(name)
                        .setSubText("" + 0 + "%")
                        .setContentIntent(pendingIntent)
                        .setProgress(100, 0, false)
                        .setOnlyAlertOnce(true)
                        .setSmallIcon(R.drawable.download)
                        .addAction(action)
                        .setCategory(Notification.CATEGORY_PROGRESS)
                        .setUsesChronometer(true)
                        .setOngoing(true);
                Notification notification = builder.build();

                notificationManager.notify(notificationId, notification);
                setForegroundAsync(new ForegroundInfo(notificationId, notification));


                if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                        Objects.equals(uri.getScheme(), Content.IPFS)) {

                    try {

                        Cid content = docs.getContent(session, uri, this::isStopped);
                        Objects.requireNonNull(content);
                        String mimeType = docs.getMimeType(session, getApplicationContext(),
                                uri, content, this::isStopped);

                        if (Objects.equals(mimeType, MimeTypeService.DIR_MIME_TYPE)) {
                            doc = doc.createDirectory(name);
                            Objects.requireNonNull(doc);
                        }

                        downloadContent(session, builder, doc, content,
                                mimeType, name, notificationId);


                        if (!success.get()) {
                            if (!isStopped()) {
                                builder.setContentText(getApplicationContext()
                                                .getString(R.string.download_canceled, name))
                                        .setSubText("")
                                        .setProgress(0, 0, false);
                                notificationManager.notify(notificationId, builder.build());
                            }
                        } else {
                            builder.setContentText(getApplicationContext().
                                            getString(R.string.download_complete, name))
                                    .setSubText("")
                                    .setProgress(0, 0, false);
                            notificationManager.notify(notificationId, builder.build());

                        }


                    } catch (Throwable e) {
                        if (!isStopped()) {
                            builder.setContentText(getApplicationContext()
                                            .getString(R.string.download_canceled, name))
                                    .setSubText("")
                                    .setProgress(0, 0, false);
                            notificationManager.notify(notificationId, builder.build());
                        }
                        throw e;
                    }
                }
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }


    private void downloadContent(@NonNull Session session, @NonNull Notification.Builder builder,
                                 @NonNull DocumentFile doc, @NonNull Cid root,
                                 @NonNull String mimeType, @NonNull String name,
                                 int notificationId) throws Exception {
        downloadLinks(session, builder, doc, root, mimeType, name, notificationId);
    }


    private void download(@NonNull Session session, @NonNull Notification.Builder builder,
                          @NonNull DocumentFile doc,
                          @NonNull Cid cid, int notificationId) throws Exception {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start [" + (System.currentTimeMillis() - start) + "]...");
        NotificationManager notificationManager = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        String name = doc.getName();
        Objects.requireNonNull(name);
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        if (!ipfs.isDir(session, cid, this::isStopped)) {

            try (OutputStream os = getApplicationContext().
                    getContentResolver().openOutputStream(doc.getUri())) {

                ipfs.fetchToOutputStream(session, os, cid, new Progress() {
                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }

                    @Override
                    public void setProgress(int progress) {
                        builder.setSubText("" + progress + "%")
                                .setContentTitle(name)
                                .setProgress(100, progress, false);
                        notificationManager.notify(notificationId, builder.build());
                    }

                });

            } catch (Throwable throwable) {
                success.set(false);

                try {
                    if (doc.exists()) {
                        doc.delete();
                    }
                } catch (Throwable throwable1) {
                    LogUtils.error(TAG, throwable1);
                }

                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }
        }
    }


    private void evalLinks(@NonNull Session session, @NonNull Notification.Builder builder,
                           @NonNull DocumentFile doc,
                           @NonNull List<Link> links, int notificationId) throws Exception {
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        for (Link link : links) {
            if (!isStopped()) {
                Cid cid = link.getCid();
                if (ipfs.isDir(session, cid, this::isStopped)) {
                    DocumentFile dir = doc.createDirectory(link.getName());
                    Objects.requireNonNull(dir);
                    downloadLinks(session, builder, dir, cid, MimeTypeService.DIR_MIME_TYPE,
                            link.getName(), notificationId);
                } else {
                    String mimeType = MimeTypeService.getMimeType(link.getName());
                    download(session, builder, Objects.requireNonNull(
                                    doc.createFile(mimeType, link.getName())),
                            cid, notificationId);
                }
            }
        }

    }


    private void downloadLinks(@NonNull Session session, @NonNull Notification.Builder builder,
                               @NonNull DocumentFile doc,
                               @NonNull Cid cid, @NonNull String mimeType,
                               @NonNull String name, int notificationId) throws Exception {

        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        List<Link> links = ipfs.links(session, cid, false, this::isStopped);

        if (links.isEmpty()) {
            if (!isStopped()) {
                DocumentFile child = doc.createFile(mimeType, name);
                Objects.requireNonNull(child);
                download(session, builder, child, cid, notificationId);
            }
        } else {
            evalLinks(session, builder, doc, links, notificationId);
        }

    }

}
