package threads.server.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.color.MaterialColors;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.files.FileInfo;
import threads.server.services.MimeTypeService;

public class FilesViewAdapter extends RecyclerView.Adapter<FilesViewAdapter.ViewHolder> implements FileItemPosition {

    private static final String TAG = FilesViewAdapter.class.getSimpleName();
    private final Context mContext;
    private final FilesAdapterListener mListener;
    private final List<FileInfo> fileInfos = new ArrayList<>();

    @Nullable
    private SelectionTracker<Long> mSelectionTracker;

    public FilesViewAdapter(@NonNull Context context, @NonNull FilesAdapterListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }


    public void setSelectionTracker(SelectionTracker<Long> selectionTracker) {
        this.mSelectionTracker = selectionTracker;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.file;
    }

    @Override
    @NonNull
    public FilesViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(this, v);
    }

    long getIdx(int position) {
        return fileInfos.get(position).getIdx();
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final FileInfo fileInfo = this.fileInfos.get(position);


        boolean isSelected = false;
        if (mSelectionTracker != null) {
            if (mSelectionTracker.isSelected(fileInfo.getIdx())) {
                isSelected = true;
            }
        }

        holder.bind(isSelected, fileInfo);
        try {
            if (isSelected) {
                int color = MaterialColors.getColor(mContext,
                        android.R.attr.colorControlHighlight, Color.GRAY);
                holder.view.setBackgroundColor(color);
            } else {
                holder.view.setBackgroundResource(android.R.color.transparent);
            }

            int resId = MimeTypeService.getMediaResource(fileInfo.getMimeType());
            holder.main_image.setImageResource(resId);


            holder.view.setOnClickListener((v) -> mListener.onClick(fileInfo));


            String title = DOCS.getCompactString(fileInfo.getName());
            holder.name.setText(title);
            String info = DOCS.getSize(fileInfo);
            holder.size.setText(info);
            Date date = new Date(fileInfo.getLastModified());
            String dateInfo = DOCS.getDate(date);
            holder.date.setText(dateInfo);

            if (fileInfo.isLeaching()) {
                holder.progress_bar.setVisibility(View.VISIBLE);
            } else {
                holder.progress_bar.setVisibility(View.INVISIBLE);
            }

            if (fileInfo.isLeaching()) {
                holder.general_action.setEnabled(true);
                holder.general_action.setIcon(AppCompatResources.getDrawable(
                        mContext, R.drawable.pause));
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokePauseAction(fileInfo)
                );

            } else if (fileInfo.isSeeding()) {
                holder.general_action.setEnabled(true);
                holder.general_action.setIcon(AppCompatResources.getDrawable(
                        mContext, R.drawable.menu_down));
                holder.general_action.setOnClickListener((v) ->
                        mListener.invokeAction(fileInfo, v)
                );
            } else {
                holder.general_action.setEnabled(false);
                holder.general_action.setIcon(AppCompatResources.getDrawable(
                        mContext, R.drawable.menu_down));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


    }


    @Override
    public int getItemCount() {
        return fileInfos.size();
    }

    public void updateData(@NonNull List<FileInfo> fileInfos) {

        final FileDiffCallback diffCallback = new FileDiffCallback(this.fileInfos, fileInfos);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.fileInfos.clear();
        this.fileInfos.addAll(fileInfos);
        diffResult.dispatchUpdatesTo(this);


    }

    public void selectAll() {
        try {
            for (FileInfo fileInfo : fileInfos) {
                if (mSelectionTracker != null) {
                    mSelectionTracker.select(fileInfo.getIdx());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Override
    public int getPosition(long idx) {
        for (int i = 0; i < fileInfos.size(); i++) {
            if (fileInfos.get(i).getIdx() == idx) {
                return i;
            }
        }
        return 0;
    }

    public interface FilesAdapterListener {

        void invokeAction(@NonNull FileInfo fileInfo, @NonNull View view);

        void onClick(@NonNull FileInfo fileInfo);

        void invokePauseAction(@NonNull FileInfo fileInfo);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final TextView name;
        final TextView date;
        final TextView size;
        final ImageView main_image;
        final MaterialButton general_action;
        final LinearProgressIndicator progress_bar;
        final FileItemDetails fileItemDetails;

        ViewHolder(FileItemPosition pos, View v) {
            super(v);
            v.setLongClickable(true);
            v.setClickable(true);
            v.setFocusable(false);
            view = v;
            name = v.findViewById(R.id.name);
            date = v.findViewById(R.id.date);
            size = v.findViewById(R.id.size);
            general_action = v.findViewById(R.id.general_action);
            progress_bar = v.findViewById(R.id.progress_bar);
            main_image = v.findViewById(R.id.main_image);
            fileItemDetails = new FileItemDetails(pos);
        }

        void bind(boolean isSelected, FileInfo fileInfo) {
            fileItemDetails.idx = fileInfo.getIdx();

            itemView.setActivated(isSelected);
        }

        @NonNull
        ItemDetailsLookup.ItemDetails<Long> getItemDetails() {
            return fileItemDetails;
        }
    }

}
