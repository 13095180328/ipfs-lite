package threads.server.utils;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import threads.server.core.files.FileInfo;

@SuppressWarnings("WeakerAccess")
public class FileDiffCallback extends DiffUtil.Callback {
    private final List<FileInfo> mOldList;
    private final List<FileInfo> mNewList;

    public FileDiffCallback(List<FileInfo> messages, List<FileInfo> fileInfos) {
        this.mOldList = messages;
        this.mNewList = fileInfos;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areItemsTheSame(mNewList.get(
                newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areContentsTheSame(mNewList.get(newItemPosition));
    }
}
