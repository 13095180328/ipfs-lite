package threads.server.utils;

@SuppressWarnings("WeakerAccess")
public interface FileItemPosition {
    int getPosition(long idx);
}
