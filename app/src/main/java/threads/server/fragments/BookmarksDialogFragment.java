package threads.server.fragments;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Comparator;
import java.util.Objects;

import threads.server.LogUtils;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.books.Bookmark;
import threads.server.model.BookmarkViewModel;
import threads.server.model.LiteViewModel;
import threads.server.utils.BookmarksViewAdapter;
import threads.server.utils.SwipeToDeleteCallback;

public class BookmarksDialogFragment extends BottomSheetDialogFragment {
    public static final String TAG = BookmarksDialogFragment.class.getSimpleName();


    private long mLastClickTime = 0;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        dialog.setContentView(R.layout.booksmark_view);

        RecyclerView bookmarks = dialog.findViewById(R.id.bookmarks);
        Objects.requireNonNull(bookmarks);
        LiteViewModel liteViewModel = new ViewModelProvider(requireActivity())
                .get(LiteViewModel.class);

        bookmarks.setLayoutManager(new LinearLayoutManager(requireContext()));
        BookmarksViewAdapter mBookmarksViewAdapter = new BookmarksViewAdapter(requireContext(),
                bookmark -> {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    try {
                        Thread.sleep(150);

                        liteViewModel.setUri(Uri.parse(bookmark.getUri()));

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        dismiss();
                    }
                });
        bookmarks.setAdapter(mBookmarksViewAdapter);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(mBookmarksViewAdapter));
        itemTouchHelper.attachToRecyclerView(bookmarks);


        BookmarkViewModel bookmarkViewModel =
                new ViewModelProvider(this).get(BookmarkViewModel.class);

        bookmarkViewModel.getBookmarks().observe(this, (marks -> {
            try {
                if (marks != null) {
                    marks.sort(Comparator.comparing(Bookmark::getTimestamp).reversed());
                    mBookmarksViewAdapter.setBookmarks(marks);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));

        return dialog;
    }
}
