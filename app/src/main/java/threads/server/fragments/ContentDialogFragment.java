package threads.server.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.cid.IPV;
import threads.lite.core.Server;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.services.MimeTypeService;

public class ContentDialogFragment extends BottomSheetDialogFragment {

    public static final String TAG = ContentDialogFragment.class.getSimpleName();
    private static final int QR_CODE_SIZE = 250;

    public static ContentDialogFragment newInstance(@NonNull String title,
                                                    @NonNull String message,
                                                    @NonNull Uri uri,
                                                    boolean showInfo) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Content.INFO, showInfo);
        bundle.putString(Content.TITLE, title);
        bundle.putString(Content.TEXT, message);
        bundle.putString(Content.URL, uri.toString());
        ContentDialogFragment fragment = new ContentDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private static Bitmap getBitmap(@NonNull String content) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(content,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        dialog.setContentView(R.layout.content_info);


        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);
        String title = bundle.getString(Content.TITLE, getString(R.string.information));
        String message = bundle.getString(Content.TEXT, "");
        String url = bundle.getString(Content.URL, "");
        boolean showInfo = bundle.getBoolean(Content.INFO);

        TextView textViewTitle = dialog.findViewById(R.id.title);
        Objects.requireNonNull(textViewTitle);
        textViewTitle.setText(title);

        ImageView imageView = dialog.findViewById(R.id.uri_qrcode);
        Objects.requireNonNull(imageView);
        TextView page = dialog.findViewById(R.id.page);
        Objects.requireNonNull(page);

        page.setCompoundDrawablePadding(8);
        if (url.isEmpty()) {
            page.setVisibility(View.GONE);
        } else {
            page.setText(url);
        }

        TextView important = dialog.findViewById(R.id.important);
        Objects.requireNonNull(important);
        TextView text = dialog.findViewById(R.id.text);
        Objects.requireNonNull(text);
        text.setText(message);

        if (showInfo) {
            try {
                Server server = DOCS.getInstance(requireContext()).getServer();
                Objects.requireNonNull(server);
                long port = server.getPort();
                Bitmap bitmapRight = MimeTypeService.getPortBitmap(requireContext(), port);

                IPV ipv = IPFS.getInstance(requireContext()).ipv().get();
                Bitmap bitmapLeft = MimeTypeService.getIPvBitmap(requireContext(), ipv);

                Drawable drawableLeft = new BitmapDrawable(getResources(), bitmapLeft);
                Drawable drawableRight = new BitmapDrawable(getResources(), bitmapRight);
                important.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        drawableLeft, null, drawableRight, null);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        } else {
            important.setVisibility(View.GONE);
        }

        Bitmap bitmap = getBitmap(url);
        imageView.setImageBitmap(bitmap);

        return dialog;
    }
}
