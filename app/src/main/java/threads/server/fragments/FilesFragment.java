package threads.server.fragments;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkManager;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.navigationrail.NavigationRailView;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.core.files.FileInfo;
import threads.server.model.LiteViewModel;
import threads.server.services.LiteService;
import threads.server.services.MimeTypeService;
import threads.server.utils.FileItemDetailsLookup;
import threads.server.utils.FilesItemKeyProvider;
import threads.server.utils.FilesViewAdapter;
import threads.server.work.CopyDirectoryWorker;
import threads.server.work.CopyFileWorker;
import threads.server.work.PageWorker;
import threads.server.work.SwarmWorker;
import threads.server.work.UploadFolderWorker;


public class FilesFragment extends Fragment implements AccessFragment,
        SwipeRefreshLayout.OnRefreshListener, FilesViewAdapter.FilesAdapterListener {

    private static final String TAG = FilesFragment.class.getSimpleName();
    private final ArrayDeque<Long> stack = new ArrayDeque<>();
    private final AtomicLong selectedIdx = new AtomicLong(0);
    private final ActivityResultLauncher<Intent> mFileExportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                Uri uri = data.getData();
                                Objects.requireNonNull(uri);
                                if (!DOCS.hasWritePermission(requireContext(), uri)) {
                                    EVENTS.getInstance(requireContext()).error(
                                            getString(R.string.file_has_no_write_permission));
                                    return;
                                }

                                CopyFileWorker.copyTo(requireContext(), uri, selectedIdx.get());

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private boolean widthMode = false;
    private long lastClickTime = 0;
    private LiteViewModel liteViewModel;
    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);

                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!DOCS.hasReadPermission(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (DOCS.isPartial(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(requireContext(),
                                                    getParentFile(), uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!DOCS.hasReadPermission(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (DOCS.isPartial(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(requireContext(),
                                                getParentFile(), uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mFilesImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);

                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();

                                    LiteService.files(requireContext(), mClipData,
                                            getParentFile());

                                } else if (data.getData() != null) {
                                    Uri uri = data.getData();
                                    Objects.requireNonNull(uri);
                                    if (!DOCS.hasReadPermission(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_has_no_read_permission));
                                        return;
                                    }

                                    if (DOCS.isPartial(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_not_valid));
                                        return;
                                    }

                                    LiteService.file(requireContext(), getParentFile(), uri);
                                }

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mDirExportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                Uri uri = data.getData();
                                Objects.requireNonNull(uri);

                                if (!DOCS.hasWritePermission(requireContext(), uri)) {
                                    EVENTS.getInstance(requireContext()).error(
                                            getString(R.string.file_has_no_write_permission));
                                    return;
                                }

                                CopyDirectoryWorker.copyTo(requireContext(), uri, getParentFile());

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private FilesViewAdapter filesViewAdapter;
    private RecyclerView recyclerView;
    private ActionMode actionMode;
    private SelectionTracker<Long> selectionTracker;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FileItemDetailsLookup fileItemDetailsLookup;
    private NavigationRailView navigationRailView;
    private ExtendedFloatingActionButton extendedFloatingActionButton;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        releaseActionMode();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null) {
            selectionTracker.onSaveInstanceState(outState);
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void findInPage() {
        try {
            if (isResumed()) {
                actionMode = ((AppCompatActivity)
                        requireActivity()).startSupportActionMode(
                        createSearchActionModeCallback());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.files_view, container, false);
    }

    private void switchDisplayModes() {
        if (!widthMode) {
            navigationRailView.setVisibility(View.GONE);
        } else {
            navigationRailView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            evaluateDisplayModes();
            switchDisplayModes();
            showFab(Objects.requireNonNull(liteViewModel.getShowFab().getValue()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickFilesAdd() {

        try {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType(MimeTypeService.ALL);
            String[] mimeTypes = {MimeTypeService.ALL};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            mFilesImportForResult.launch(intent);

        } catch (Throwable throwable) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
            LogUtils.error(TAG, throwable);
        }
    }

    private void showFab(boolean showFab) {
        if (widthMode) {
            extendedFloatingActionButton.hide();
        } else {
            if (showFab) {
                extendedFloatingActionButton.show();
            } else {
                extendedFloatingActionButton.hide();
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        extendedFloatingActionButton = view.findViewById(R.id.floating_action_button);


        extendedFloatingActionButton.setOnClickListener((v) -> {

            if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            clickFilesAdd();

        });


        navigationRailView = view.findViewById(R.id.navigation_rail);
        switchDisplayModes();


        navigationRailView.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            if (id == R.id.menu_add_file) {
                try {
                    clickFilesAdd();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_new_text) {
                try {
                    TextDialogFragment.newInstance(liteViewModel.getParentFileIdxValue()).
                            show(getChildFragmentManager(), TextDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_import_folder) {
                try {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                    mFolderImportForResult.launch(intent);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_new_folder) {
                try {
                    NewFolderDialogFragment.newInstance(liteViewModel.getParentFileIdxValue()).
                            show(getChildFragmentManager(), NewFolderDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_multiaddrs) {
                try {
                    showMultiaddrs();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
            return false;
        });

        MaterialCardView homepageUri = view.findViewById(R.id.homepage_uri);
        homepageUri.setOnClickListener(v -> {
            try {
                DOCS docs = DOCS.getInstance(requireContext());
                Uri uri = docs.getHomePageUri();
                ContentDialogFragment.newInstance(getString(R.string.homepage),
                                getString(R.string.connection_text),
                                uri, true)
                        .show(getChildFragmentManager(), ContentDialogFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        MaterialTextView textHomepageUriTitle = view.findViewById(R.id.text_homepage_uri_title);
        textHomepageUriTitle.setText(getString(R.string.homepage));
        MaterialTextView textHomepageUri = view.findViewById(R.id.text_homepage_uri);

        try {
            DOCS docs = DOCS.getInstance(requireContext());
            textHomepageUri.setText(docs.getHomePageUri().toString());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        TextInputLayout dropDownDirectoryMenu = view.findViewById(R.id.dropdown_directory_menu);


        liteViewModel = new ViewModelProvider(requireActivity()).get(LiteViewModel.class);

        liteViewModel.getShowFab().observe(getViewLifecycleOwner(), (showFab) -> {
            try {
                if (showFab != null) {
                    showFab(showFab);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        liteViewModel.getParentFileIdx().observe(getViewLifecycleOwner(), (parent) -> {
            if (parent != null) {
                if (parent == 0L) {
                    dropDownDirectoryMenu.setVisibility(View.GONE);
                    homepageUri.setVisibility(View.VISIBLE);
                } else {
                    homepageUri.setVisibility(View.GONE);
                    dropDownDirectoryMenu.setVisibility(View.VISIBLE);
                    fillDropDownDirectory(dropDownDirectoryMenu, parent);
                }
            }
        });


        recyclerView = view.findViewById(R.id.recycler_view_message_list);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setItemAnimator(null);

        filesViewAdapter = new FilesViewAdapter(requireContext(), this);
        recyclerView.setAdapter(filesViewAdapter);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean hasSelection = selectionTracker.hasSelection();
                if (dy > 0) {
                    liteViewModel.setShowFab(false);
                } else if (dy < 0 && !hasSelection) {
                    liteViewModel.setShowFab(true);
                }

                swipeRefreshLayout.setEnabled
                        (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });

        fileItemDetailsLookup = new FileItemDetailsLookup(recyclerView);


        swipeRefreshLayout = view.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);


        selectionTracker = new SelectionTracker.Builder<>(TAG, recyclerView,
                new FilesItemKeyProvider(filesViewAdapter),
                fileItemDetailsLookup,
                StorageStrategy.createLongStorage())
                .build();


        selectionTracker.addObserver(new SelectionTracker.SelectionObserver<>() {
            @Override
            public void onSelectionChanged() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionRestored();
            }
        });

        filesViewAdapter.setSelectionTracker(selectionTracker);


        liteViewModel.getLiveDataFiles().observe(getViewLifecycleOwner(), (fileInfos) -> {
            if (fileInfos != null) {
                try {
                    int size = filesViewAdapter.getItemCount();
                    boolean scrollToTop = size < fileInfos.size();

                    filesViewAdapter.updateData(fileInfos);

                    if (scrollToTop) {
                        try {
                            recyclerView.scrollToPosition(0);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        });


        if (savedInstanceState != null) {
            selectionTracker.onRestoreInstanceState(savedInstanceState);
        } else {
            evaluateDisplayModes();
            switchDisplayModes();
        }

    }

    private void fillDropDownDirectory(TextInputLayout dropDownDirectoryMenu, long directory) {


        try {
            FILES files = FILES.getInstance(requireContext());
            List<FileInfo> ancestors = files.getAncestors(directory);
            int size = ancestors.size();

            String[] names = new String[size + 1];
            long[] indices = new long[size + 1];

            names[0] = getString(R.string.homepage);
            indices[0] = 0L;

            for (int i = 0; i < size; i++) {
                FileInfo fileInfo = ancestors.get(i);
                names[i + 1] = fileInfo.getName();
                indices[i + 1] = fileInfo.getIdx();
            }

            MaterialAutoCompleteTextView textView = (MaterialAutoCompleteTextView)
                    dropDownDirectoryMenu.getEditText();
            Objects.requireNonNull(textView);
            textView.setSimpleItems(names);

            textView.setText(names[size], false);

            textView.setOnItemClickListener((parent, view, position, id) -> {
                long idx = indices[position];
                if (idx != directory) {
                    liteViewModel.setParentFileIdx(idx);
                }
                textView.dismissDropDown();
            });
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void showMultiaddrs() throws Exception {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.multiaddrs);

        String link = "";
        DOCS docs = DOCS.getInstance(requireContext());
        for (Multiaddr multiaddr : docs.dialableAddresses()) {
            link = link.concat("\n").concat(multiaddr.toString()).concat("\n");
        }
        builder.setMessage(link);

        builder.setPositiveButton(getString(android.R.string.ok),
                (dialogInterface, which) -> dialogInterface.cancel());
        String finalLink = link;
        builder.setNeutralButton(getString(android.R.string.copy),
                (dialogInterface, which) -> {
                    LogUtils.error(TAG, finalLink);
                    ClipboardManager clipboardManager = (ClipboardManager)
                            requireContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(
                            getString(R.string.multiaddrs), finalLink);
                    clipboardManager.setPrimaryClip(clipData);
                });
        builder.show();
    }

    private long[] convert(Selection<Long> entries) {
        int i = 0;

        long[] basic = new long[entries.size()];
        for (Long entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    private void deleteAction() {

        final EVENTS events = EVENTS.getInstance(requireContext());

        if (!selectionTracker.hasSelection()) {
            events.warning(getString(R.string.no_marked_file_delete));
            return;
        }


        try {
            long[] entries = convert(selectionTracker.getSelection());

            selectionTracker.clearSelection();

            removeFiles(entries);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void removeFiles(long... indices) {

        FILES files = FILES.getInstance(requireContext());
        EVENTS events = EVENTS.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            long start = System.currentTimeMillis();

            try {
                files.setDeleting(indices);

                events.delete(Arrays.toString(indices));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }


    @Override
    public void invokeAction(@NonNull FileInfo fileInfo, @NonNull View view) {

        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();

        try {
            PopupMenu menu = new PopupMenu(requireContext(), view);
            menu.inflate(R.menu.popup_file_menu);
            menu.getMenu().findItem(R.id.popup_share).setVisible(true);
            menu.getMenu().findItem(R.id.popup_delete).setVisible(true);
            menu.getMenu().findItem(R.id.popup_copy_to).setVisible(fileInfo.isSeeding());

            menu.setOnMenuItemClickListener((item) -> {

                if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                if (item.getItemId() == R.id.popup_info) {
                    clickFileInfo(fileInfo);
                    return true;
                } else if (item.getItemId() == R.id.popup_delete) {
                    clickFileDelete(fileInfo.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_share) {
                    clickFileShare(fileInfo.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_copy_to) {
                    clickFileCopy(fileInfo);
                    return true;
                } else {
                    return false;
                }
            });

            menu.show();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


    }

    private void clickFileShare(long idx) {
        EVENTS events = EVENTS.getInstance(requireContext());
        FILES files = FILES.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                FileInfo fileInfo = files.getFileInfo(idx);
                Objects.requireNonNull(fileInfo);
                ComponentName[] names = {new ComponentName(
                        requireContext().getApplicationContext(), MainActivity.class)};
                Uri uri = DOCS.getInstance(requireContext()).getIpnsPath(fileInfo, false);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
                intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                intent.putExtra(Intent.EXTRA_SUBJECT, fileInfo.getName());
                intent.putExtra(Intent.EXTRA_TITLE, fileInfo.getName());


                Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(chooser);


            } catch (Throwable ignore) {
                events.warning(getString(R.string.no_activity_found_to_handle_uri));
            }
        });


    }

    private void clickFileCopy(@NonNull FileInfo fileInfo) {
        try {
            if (fileInfo.isDir()) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                mDirExportForResult.launch(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                intent.setType(fileInfo.getMimeType());
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra(Intent.EXTRA_TITLE, fileInfo.getName());
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                selectedIdx.set(fileInfo.getIdx());
                mFileExportForResult.launch(intent);
            }
        } catch (Throwable e) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }
    }

    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_files_action_mode, menu);

                liteViewModel.setShowFab(false);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int itemId = item.getItemId();
                if (itemId == R.id.action_mode_mark_all) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    filesViewAdapter.selectAll();

                    return true;
                } else if (itemId == R.id.action_mode_delete) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    deleteAction();

                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                selectionTracker.clearSelection();

                liteViewModel.setShowFab(true);

                if (actionMode != null) {
                    actionMode = null;
                }

            }
        };

    }

    @Override
    public void onClick(@NonNull FileInfo fileInfo) {

        if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();

        try {
            if (!selectionTracker.hasSelection()) {

                if (actionMode != null) {
                    actionMode.finish();
                    actionMode = null;
                }

                if (fileInfo.isDir()) {
                    stack.push(fileInfo.getParent());
                    liteViewModel.setParentFileIdx(fileInfo.getIdx());
                } else {
                    clickThreadPlay(fileInfo);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void clickFileInfo(@NonNull FileInfo fileInfo) {

        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                DOCS docs = DOCS.getInstance(requireContext());
                Uri uri = docs.getIpnsPath(fileInfo, false);

                ContentDialogFragment.newInstance(getString(R.string.information),
                                getString(R.string.url_data_access, fileInfo.getName()),
                                uri, false)
                        .show(getChildFragmentManager(), ContentDialogFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


    }

    private void evaluateDisplayModes() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;
        widthMode = widthDp >= 600;
    }

    private void clickThreadPlay(@NonNull FileInfo fileInfo) {

        EVENTS events = EVENTS.getInstance(requireContext());

        if (fileInfo.isSeeding()) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                try {
                    DOCS docs = DOCS.getInstance(requireContext());
                    Cid cid = fileInfo.getCid();
                    Objects.requireNonNull(cid);

                    String mimeType = fileInfo.getMimeType();

                    // special case
                    if (Objects.equals(mimeType, MimeTypeService.URL_MIME_TYPE)) {
                        IPFS ipfs = IPFS.getInstance(requireContext());
                        Uri uri = Uri.parse(ipfs.getText(docs.getSession(), cid, () -> false));
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        return;
                    } else if (Objects.equals(mimeType, MimeTypeService.HTML_MIME_TYPE)) {
                        Uri uri = docs.getIpnsPath(fileInfo, false);
                        liteViewModel.setUri(uri);
                        return;
                    }
                    Uri uri = docs.getIpnsPath(fileInfo, true);
                    //Uri uri = DOCS.getInstance(mContext).getPath(thread);
                    liteViewModel.setUri(uri);


                } catch (Throwable ignore) {
                    events.warning(getString(R.string.no_activity_found_to_handle_uri));
                }
            });
        }
    }

    @Override
    public void invokePauseAction(@NonNull FileInfo fileInfo) {

        if (SystemClock.elapsedRealtime() - lastClickTime < Settings.CLICK_OFFSET) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();


        UUID uuid = fileInfo.getWorkUUID();
        if (uuid != null) {
            WorkManager.getInstance(requireContext()).cancelWorkById(uuid);
        }

        FILES files = FILES.getInstance(requireContext());
        Executors.newSingleThreadExecutor().submit(() ->
                files.resetLeaching(fileInfo.getIdx()));

    }


    private int dp48ToPixels() {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) 48 * density);
    }

    private void clickFileDelete(long idx) {
        removeFiles(idx);
    }


    private ActionMode.Callback createSearchActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_search_action_mode, menu);

                fileItemDetailsLookup.setActive(false);

                MenuItem searchMenuItem = menu.findItem(R.id.action_search);

                SearchView mSearchView = (SearchView) searchMenuItem.getActionView();

                TextView textView = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_src_text);
                textView.setTextSize(16);
                textView.setMinHeight(dp48ToPixels());

                mSearchView.setIconifiedByDefault(false);
                mSearchView.setFocusable(true);
                mSearchView.setFocusedByDefault(true);
                String query = liteViewModel.getQuery().getValue();
                Objects.requireNonNull(query);
                mSearchView.setQuery(query, true);
                mSearchView.setIconified(false);
                mSearchView.requestFocus();


                mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        liteViewModel.getQuery().setValue(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        liteViewModel.getQuery().setValue(newText);
                        return false;
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                try {
                    fileItemDetailsLookup.setActive(true);
                    liteViewModel.setQuery("");

                    if (actionMode != null) {
                        actionMode = null;
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        };

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);

        try {
            if (!Network.isNetworkConnected(requireContext())) {
                EVENTS.getInstance(requireContext()).warning(
                        getString(R.string.publish_no_network));
            } else if (DOCS.getInstance(requireContext()).globalPublishingEnabled()) {
                EVENTS.getInstance(requireContext()).warning(getString(R.string.publish_homepage));
                PageWorker.publish(requireContext(), 3);
            } else if (DOCS.getInstance(requireContext()).relayPublishingEnabled()) {
                EVENTS.getInstance(requireContext()).warning(getString(R.string.publish_homepage));
                SwarmWorker.reservations(requireContext(), ExistingWorkPolicy.REPLACE, 0);
            } else {
                EVENTS.getInstance(requireContext()).warning(
                        getString(R.string.publish_not_global));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void releaseActionMode() {
        try {
            if (isResumed()) {
                if (actionMode != null) {
                    actionMode.finish();
                    actionMode = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private long getParentFile() {
        return liteViewModel.getParentFileIdxValue();
    }

    public boolean onBackPressed() {
        if (!stack.isEmpty()) {
            liteViewModel.setParentFileIdx(stack.pop());
            return true;
        }
        return false;
    }
}
