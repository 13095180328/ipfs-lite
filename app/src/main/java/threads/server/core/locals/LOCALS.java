package threads.server.core.locals;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public class LOCALS {
    private static volatile LOCALS INSTANCE = null;
    @NonNull
    private final ConcurrentHashMap<PeerId, Multiaddr> locals = new ConcurrentHashMap<>();

    public static LOCALS getInstance() throws Exception {

        if (INSTANCE == null) {
            synchronized (LOCALS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new LOCALS();
                }
            }
        }
        return INSTANCE;
    }

    public void addLocalAddress(@NonNull Multiaddr multiaddr) {
        locals.put(multiaddr.getPeerId(), multiaddr);
    }

    @Nullable
    public Multiaddr getLocalAddress(@NonNull PeerId peerId) {
        return locals.get(peerId);
    }

    public void removeLocalAddress(@NonNull PeerId peerId) {
        locals.remove(peerId);
    }

    @NonNull
    public Set<Multiaddr> getLocals() {
        return new HashSet<>(locals.values());
    }
}
