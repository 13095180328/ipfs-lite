package threads.server.core.files;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import threads.lite.cid.Cid;

@Dao
public interface FileInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertFileInfo(FileInfo fileInfo);

    @Query("SELECT cid FROM FileInfo WHERE idx = :idx")
    Cid getContent(long idx);

    @Query("UPDATE FileInfo SET leaching = 1 WHERE idx = :idx")
    void setLeaching(long idx);

    @Query("UPDATE FileInfo SET leaching = 0 WHERE idx = :idx")
    void resetLeaching(long idx);

    @Query("UPDATE FileInfo SET deleting = 1 WHERE idx = :idx")
    void setDeleting(long idx);

    @Query("UPDATE FileInfo SET deleting = 0 WHERE idx = :idx")
    void resetDeleting(long idx);

    @Query("SELECT * FROM FileInfo WHERE idx =:idx")
    FileInfo getFileInfo(long idx);

    @Query("SELECT * FROM FileInfo WHERE parent =:parent AND deleting = 0 AND name LIKE :query ORDER BY lastModified DESC")
    LiveData<List<FileInfo>> getLiveDataFiles(long parent, String query);

    @Query("UPDATE FileInfo SET cid =:cid, size = :size, lastModified =:lastModified  WHERE idx = :idx")
    void updateContent(long idx, Cid cid, long size, long lastModified);

    @Query("SELECT * FROM FileInfo WHERE parent = 0 AND deleting = 0 AND seeding = 1")
    List<FileInfo> getPins();

    @Query("SELECT * FROM FileInfo WHERE parent =:parent AND deleting = 0")
    List<FileInfo> getChildren(long parent);

    @Query("UPDATE FileInfo SET seeding = 1, leaching = 0 WHERE idx = :idx")
    void setDone(long idx);

    @Query("UPDATE FileInfo SET cid =:cid, seeding = 1, leaching = 0, work = null WHERE idx = :idx")
    void setDone(long idx, Cid cid);

    @Query("UPDATE FileInfo SET work = :work WHERE idx = :idx")
    void setWork(long idx, String work);

    @Query("DELETE FROM FileInfo WHERE idx = :idx")
    void delete(long idx);

    @Query("UPDATE FileInfo SET uri =:uri WHERE idx = :idx")
    void setUri(long idx, String uri);

}
