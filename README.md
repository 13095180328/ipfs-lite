# IPFS Lite

IPFS Lite is an application to support the standard use cases of IPFS

## General

The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/threads.server/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
alt="Get it on Google Play"
height="80">](https://play.google.com/store/apps/details?id=threads.server)

## Documentation

**IPFS Lite** based on a subset of IPFS (https://ipfs.io/), which is described in detail
in the **IPFS Lite Library** (https://gitlab.com/remmer.wilts/threads-lite/)

## Links

[Privacy Policy](https://gitlab.com/remmer.wilts/ipfs-lite/-/blob/master/POLICY.md)
<br/>
[Apache License](https://gitlab.com/remmer.wilts/ipfs-lite/-/blob/master/LICENSE)
