/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.recovery;

import androidx.annotation.NonNull;

import net.luminis.LogUtils;
import net.luminis.quic.EncryptionLevel;
import net.luminis.quic.FrameProcessor2;
import net.luminis.quic.HandshakeState;
import net.luminis.quic.HandshakeStateListener;
import net.luminis.quic.PnSpace;
import net.luminis.quic.Role;
import net.luminis.quic.cc.CongestionController;
import net.luminis.quic.frame.AckFrame;
import net.luminis.quic.frame.Padding;
import net.luminis.quic.frame.PingFrame;
import net.luminis.quic.frame.QuicFrame;
import net.luminis.quic.packet.QuicPacket;
import net.luminis.quic.send.Sender;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * QUIC Loss Detection is specified in <a href="https://www.rfc-editor.org/rfc/rfc9002.html">...</a>.
 * <p>
 * "QUIC senders use acknowledgments to detect lost packets and a PTO to ensure acknowledgments are received"
 * It uses a single timer, because either there are lost packets to detect, or a probe must be scheduled, never both.
 *
 * <b>Ack based loss detection</b>
 * When an Ack is received, packets that are sent "long enough" before the largest acked, are deemed lost; for the
 * packets not send "long enough", a timer is set to mark them as lost when "long enough" time has been passed.
 * <p>
 * An example:
 * -----------------------time------------------->>
 * sent:   1           2      3        4
 * acked:                                    4
 * \--- long enough before 4 --/                       => 1 is marked lost immediately
 * \--not long enough before 4 --/
 * |
 * Set timer at this point in time, as that will be "long enough".
 * At that time, a new timer will be set for 3, unless acked meanwhile.
 *
 * <b>Detecting tail loss with probe timeout</b>
 * When no Acks arrive, no packets will be marked as lost. To trigger the peer to send an ack (so loss detection can do
 * its job again), a probe (ack-eliciting packet) will be sent after the probe timeout. If the situation does not change
 * (i.e. no Acks received), additional probes will be sent, but with an exponentially growing delay.
 * <p>
 * An example:
 * -----------------------time------------------->>
 * sent:   1           2      3        4
 * acked:                                    4
 * \-- timer set at loss time  --/
 * |
 * When the timer fires, there is no new ack received, so
 * nothing can be marked as lost. A probe is scheduled for
 * "probe timeout" time after the time 3 was sent:
 * \-- timer set at "probe timeout" time after 3 was sent --\
 * |
 * Send probe!
 * <p>
 * Note that packet 3 will not be marked as lost as long no ack is received!
 *
 * <b>Exceptions</b>
 * Because a server might be blocked by the anti-amplification limit, a client must also send probes when it has no
 * ack eliciting packets in flight, but is not sure whether the peer has validated the client address.
 */
public class RecoveryManager implements FrameProcessor2<AckFrame>, HandshakeStateListener {

    private static final String TAG = RecoveryManager.class.getSimpleName();
    private final Role role;
    private final RttEstimator rttEstimater;
    private final LossDetector[] lossDetectors = new LossDetector[PnSpace.pnSpaces().size()];
    private final Sender sender;
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private final ReentrantLock scheduleLock = new ReentrantLock();
    private final AtomicInteger ptoCount = new AtomicInteger(0);
    private final AtomicReference<Instant> timerExpiration = new AtomicReference<>();
    private final AtomicReference<HandshakeState> handshakeState =
            new AtomicReference<>(HandshakeState.Initial);
    private final AtomicBoolean reset = new AtomicBoolean(false);
    private int receiverMaxAckDelay; // TODO never set
    private ScheduledFuture<?> lossDetectionFuture;  // Concurrency: guarded by scheduleLock

    public RecoveryManager(Role role, RttEstimator rttEstimater,
                           CongestionController congestionController, Sender sender) {
        this.role = role;
        this.rttEstimater = rttEstimater;
        for (PnSpace pnSpace : PnSpace.pnSpaces()) {
            lossDetectors[pnSpace.ordinal()] = new LossDetector(this, rttEstimater,
                    congestionController, sender::flush);
        }
        this.sender = sender;
        this.lossDetectionFuture = new NullScheduledFuture();
    }

    void setLossDetectionTimer() {
        PnSpaceTime earliestLossTime = getEarliestLossTime(LossDetector::getLossTime);
        Instant lossTime = earliestLossTime != null ? earliestLossTime.lossTime : null;
        if (lossTime != null) {
            rescheduleLossDetectionTimeout(lossTime);
        } else {
            boolean ackElicitingInFlight = ackElicitingInFlight();
            boolean peerAwaitingAddressValidation = peerAwaitingAddressValidation();
            // https://datatracker.ietf.org/doc/html/draft-ietf-quic-recovery-34#section-6.2.2.1
            // "That is, the client MUST set the probe timer if the client has not received an acknowledgment for any of
            //  its Handshake packets and the handshake is not confirmed (...), even if there are no packets in flight."
            if (ackElicitingInFlight || peerAwaitingAddressValidation) {
                PnSpaceTime ptoTimeAndSpace = getPtoTimeAndSpace();
                if (ptoTimeAndSpace == null) {
                    LogUtils.verbose(TAG, "cancelling loss detection timer (no loss time set, no ack eliciting in flight, peer not awaiting address validation (1))");
                    unschedule();
                } else {
                    rescheduleLossDetectionTimeout(ptoTimeAndSpace.lossTime);

                    if (LogUtils.isDebug()) {
                        int timeout = (int) Duration.between(Instant.now(), ptoTimeAndSpace.lossTime).toMillis();
                        LogUtils.verbose(TAG, "reschedule loss detection timer for PTO over " + timeout + " millis, "
                                + "based on %s/" + ptoTimeAndSpace.pnSpace + ", because "
                                + (peerAwaitingAddressValidation ? "peerAwaitingAddressValidation " : "")
                                + (ackElicitingInFlight ? "ackElicitingInFlight " : "")
                                + "| RTT:" + rttEstimater.getSmoothedRtt() + "/" + rttEstimater.getRttVar());
                    }
                }
            } else {
                LogUtils.verbose(TAG, "cancelling loss detection timer (no loss time set," +
                        " no ack eliciting in flight, peer not awaiting address validation (2))");
                unschedule();
            }
        }
    }

    /**
     * Determines the current probe timeout.
     * This method is defined in <a href="https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection-">...</a>.
     *
     * @return a <code>PnSpaceTime</code> object defining the next probe: its time and for which packet number space.
     */
    private PnSpaceTime getPtoTimeAndSpace() {
        int ptoDuration = rttEstimater.getSmoothedRtt() + Integer.max(1, 4 * rttEstimater.getRttVar());
        ptoDuration *= (int) (Math.pow(2, ptoCount.get()));

        // The pseudo code in https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection- test for
        // ! ackElicitingInFlight() to determine whether peer is awaiting address validation. In a multi-threaded
        // implementation, that solution is subject to all kinds of race conditions, so its better to just check:
        HandshakeState handshakeStateCwnd = handshakeState.get();
        if (peerAwaitingAddressValidation()) {
            if (handshakeStateCwnd.hasNoHandshakeKeys()) {
                LogUtils.verbose(TAG, "getPtoTimeAndSpace: no ack eliciting in flight and no handshake keys -> probe Initial");
                return new PnSpaceTime(PnSpace.Initial, Instant.now().plusMillis(ptoDuration));
            } else {
                LogUtils.verbose(TAG, "getPtoTimeAndSpace: no ack eliciting in flight but handshake keys -> probe Handshake");
                return new PnSpaceTime(PnSpace.Handshake, Instant.now().plusMillis(ptoDuration));
            }
        }

        // Find earliest pto time
        Instant ptoTime = Instant.MAX;
        PnSpace ptoSpace = null;
        for (PnSpace pnSpace : PnSpace.pnSpaces()) {
            if (lossDetectors[pnSpace.ordinal()].ackElicitingInFlight()) {
                if (pnSpace == PnSpace.App && handshakeStateCwnd.isNotConfirmed()) {
                    // https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection-
                    // "Skip Application Data until handshake confirmed"
                    LogUtils.verbose(TAG, "getPtoTimeAndSpace is skipping level App, because handshake not yet confirmed!");
                    continue;  // Because App is the last, this is effectively a return.
                }
                if (pnSpace == PnSpace.App) {
                    // https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection-
                    // "Include max_ack_delay and backoff for Application Data"
                    ptoDuration += receiverMaxAckDelay * (int) (Math.pow(2, ptoCount.get()));
                }
                Instant lastAckElicitingSent = lossDetectors[pnSpace.ordinal()].getLastAckElicitingSent();
                if (lastAckElicitingSent != null && lastAckElicitingSent.plusMillis(ptoDuration).isBefore(ptoTime)) {
                    ptoTime = lastAckElicitingSent.plusMillis(ptoDuration);
                    ptoSpace = pnSpace;
                }
            }
        }

        if (ptoSpace != null) {
            return new PnSpaceTime(ptoSpace, ptoTime);
        } else {
            return null;
        }
    }

    private boolean peerAwaitingAddressValidation() {
        return role == Role.Client && handshakeState.get().isNotConfirmed()
                && lossDetectors[PnSpace.Handshake.ordinal()].noAckedReceived();
    }

    private void lossDetectionTimeout() {
        // Because cancelling the ScheduledExecutor task quite often fails, double check whether the timer should expire.
        Instant expiration = timerExpiration.get();
        if (expiration == null) {
            // Timer was cancelled, but it still fired; ignore
            LogUtils.warning(TAG, "Loss detection timeout: Timer was cancelled.");
            return;
        } else if (Instant.now().isBefore(expiration) && Duration.between(Instant.now(), expiration).toMillis() > 0) {
            // Might be due to an old task that was cancelled, but unfortunately, it also happens that the scheduler
            // executes tasks much earlier than requested (30 ~ 40 ms). In that case, rescheduling is necessary to avoid
            // losing the loss detection timeout event.
            // To be sure the latest timer expiration is used, use timerExpiration i.s.o. the expiration of this call.
            LogUtils.warning(TAG, String.format("Loss detection timeout running (at %s) is %s ms too early; rescheduling to %s",
                    Instant.now(), Duration.between(Instant.now(), expiration).toMillis(), expiration));
            rescheduleLossDetectionTimeout(expiration);
        }

        PnSpaceTime earliestLossTime = getEarliestLossTime(LossDetector::getLossTime);
        Instant lossTime = earliestLossTime != null ? earliestLossTime.lossTime : null;
        if (lossTime != null) {
            lossDetectors[earliestLossTime.pnSpace.ordinal()].detectLostPackets();
            sender.flush();
            setLossDetectionTimer();
        } else {
            sendProbe();
            // Calling setLossDetectionTimer here not necessary, because the event of sending the probe will trigger it anyway.
            // And if done here, time of last-ack-eliciting might not be set yet (because packets are sent async), leading to trouble.
        }
    }

    private void sendProbe() {
        if (LogUtils.isDebug()) {
            PnSpaceTime earliestLastAckElicitingSentTime = getEarliestLossTime(LossDetector::getLastAckElicitingSent);
            if (earliestLastAckElicitingSentTime != null) {
                LogUtils.verbose(TAG, String.format(Locale.US, "Sending probe %d, because no ack since %%s. Current RTT: %d/%d.", ptoCount.get(), rttEstimater.getSmoothedRtt(), rttEstimater.getRttVar()));
            } else {
                LogUtils.verbose(TAG, String.format(Locale.US, "Sending probe %d. Current RTT: %d/%d.", ptoCount.get(), rttEstimater.getSmoothedRtt(), rttEstimater.getRttVar()));
            }
        }

        int nrOfProbes = ptoCount.incrementAndGet() > 1 ? 2 : 1;

        if (ackElicitingInFlight()) {
            PnSpaceTime ptoTimeAndSpace = getPtoTimeAndSpace();
            if (ptoTimeAndSpace == null) {
                // So, the "ack eliciting in flight" has just been acked; a new timeout will be set, no need to send a probe now
                LogUtils.verbose(TAG, "Refraining from sending probe because received ack meanwhile");
                return;
            }
            sendOneOrTwoAckElicitingPackets(ptoTimeAndSpace.pnSpace, nrOfProbes);
        } else {
            // Must be the peer awaiting address validation or race condition
            if (peerAwaitingAddressValidation()) {
                LogUtils.verbose(TAG, "Sending probe because peer awaiting address validation");
                // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-6.2.2.1
                // "When the PTO fires, the client MUST send a Handshake packet if it has Handshake keys, otherwise it
                //  MUST send an Initial packet in a UDP datagram with a payload of at least 1200 bytes."
                if (handshakeState.get().hasNoHandshakeKeys()) {
                    sendOneOrTwoAckElicitingPackets(PnSpace.Initial, 1);
                } else {
                    sendOneOrTwoAckElicitingPackets(PnSpace.Handshake, 1);
                }
            } else {
                LogUtils.verbose(TAG, "Refraining from sending probe as no ack eliciting in flight and no peer awaiting address validation");
            }
        }
    }

    private void sendOneOrTwoAckElicitingPackets(PnSpace pnSpace, int numberOfPackets) {
        if (pnSpace == PnSpace.Initial) {
            List<QuicFrame> framesToRetransmit = getFramesToRetransmit(PnSpace.Initial);
            if (!framesToRetransmit.isEmpty()) {
                LogUtils.verbose(TAG, "(Probe is an initial retransmit)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sender.sendProbe(framesToRetransmit, EncryptionLevel.Initial);
                }
            } else {
                // This can happen, when the probe is sent because of peer awaiting address validation
                LogUtils.verbose(TAG, "(Probe is Initial ping, because there is no Initial data to retransmit)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sender.sendProbe(List.of(new PingFrame(), new Padding(2)), EncryptionLevel.Initial);
                }
            }
        } else if (pnSpace == PnSpace.Handshake) {
            // Client role: find ack eliciting handshake packet that is not acked and retransmit its contents.
            List<QuicFrame> framesToRetransmit = getFramesToRetransmit(PnSpace.Handshake);
            if (!framesToRetransmit.isEmpty()) {
                LogUtils.verbose(TAG, "(Probe is a handshake retransmit)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sender.sendProbe(framesToRetransmit, EncryptionLevel.Handshake);
                }
            } else {
                LogUtils.verbose(TAG, "(Probe is a handshake ping)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sender.sendProbe(List.of(new PingFrame(), new Padding(2)), EncryptionLevel.Handshake);
                }
            }
        } else {
            EncryptionLevel probeLevel = pnSpace.relatedEncryptionLevel();
            List<QuicFrame> framesToRetransmit = getFramesToRetransmit(pnSpace);
            if (!framesToRetransmit.isEmpty()) {
                LogUtils.verbose(TAG, ("(Probe is retransmit on level " + probeLevel + ")"));
                for (int i = 0; i < numberOfPackets; i++) {
                    sender.sendProbe(framesToRetransmit, probeLevel);
                }
            } else {
                LogUtils.verbose(TAG, ("(Probe is ping on level " + probeLevel + ")"));
                for (int i = 0; i < numberOfPackets; i++) {
                    sender.sendProbe(List.of(new PingFrame(), new Padding(2)), probeLevel);
                }
            }
        }
    }

    List<QuicFrame> getFramesToRetransmit(PnSpace pnSpace) {
        List<QuicPacket> unAckedPackets = lossDetectors[pnSpace.ordinal()].unAcked();
        Optional<QuicPacket> ackEliciting = unAckedPackets.stream()
                .filter(QuicPacket::isAckEliciting)
                // Filter out Ping packets, ie. packets consisting of PingFrame's, padding and AckFrame's only.
                .filter(p -> !p.getFrames().stream().allMatch(frame -> frame instanceof PingFrame
                        || frame instanceof Padding || frame instanceof AckFrame))
                .findFirst();
        return ackEliciting.map(quicPacket -> quicPacket.getFrames().stream()
                .filter(frame -> !(frame instanceof AckFrame))
                .collect(Collectors.toList())).orElse(Collections.emptyList());
    }

    PnSpaceTime getEarliestLossTime(Function<LossDetector, Instant> pnSpaceTimeFunction) {
        PnSpaceTime earliestLossTime = null;
        for (PnSpace pnSpace : PnSpace.pnSpaces()) {
            Instant pnSpaceLossTime = pnSpaceTimeFunction.apply(lossDetectors[pnSpace.ordinal()]);
            if (pnSpaceLossTime != null) {
                if (earliestLossTime == null) {
                    earliestLossTime = new PnSpaceTime(pnSpace, pnSpaceLossTime);
                } else {
                    if (!earliestLossTime.lossTime.isBefore(pnSpaceLossTime)) {
                        earliestLossTime = new PnSpaceTime(pnSpace, pnSpaceLossTime);
                    }
                }
            }
        }
        return earliestLossTime;
    }

    private void rescheduleLossDetectionTimeout(Instant scheduledTime) {
        if (reset.get()) {
            return;
        }
        scheduleLock.lock();
        try {
            // Cancelling the current future and setting the new must be in a sync'd block
            // to ensure the right future is cancelled
            lossDetectionFuture.cancel(false);
            timerExpiration.set(scheduledTime);
            long delay = Duration.between(Instant.now(), scheduledTime).toMillis();
            // Delay can be 0 or negative, but that's no problem for ScheduledExecutorService:
            // "Zero and negative delays are also allowed, and are treated as requests for immediate execution."
            if (!scheduler.isShutdown()) {
                lossDetectionFuture = scheduler.schedule(this::runLossDetectionTimeout,
                        delay, TimeUnit.MILLISECONDS);
            }
        } catch (Throwable ignore) {
            // Can happen if has been reset concurrently
        } finally {
            scheduleLock.unlock();
        }
    }

    private void runLossDetectionTimeout() {
        try {
            lossDetectionTimeout();
        } catch (Exception error) {
            LogUtils.error(TAG, "Runtime exception occurred while running " +
                    "loss detection timeout handler", error);
        }
    }


    void unschedule() {
        lossDetectionFuture.cancel(true);
        timerExpiration.set(null);
    }

    public void onAckReceived(AckFrame ackFrame, PnSpace pnSpace, Instant timeReceived) {
        if (!reset.get()) {
            if (ptoCount.get() > 0) {
                // https://datatracker.ietf.org/doc/html/draft-ietf-quic-recovery-34#section-6.2.1
                // "To protect such a server from repeated client probes, the PTO backoff is not reset at a client that
                //  is not yet certain that the server has finished validating the client's address.
                if (!peerAwaitingAddressValidation()) {
                    ptoCount.set(0);
                } else {
                    LogUtils.verbose(TAG, "probe count not reset on ack because handshake not yet confirmed");
                }
            }
            lossDetectors[pnSpace.ordinal()].onAckReceived(ackFrame, timeReceived);
        }
    }

    public void packetSent(QuicPacket packet, Instant sent, Consumer<QuicPacket> packetLostCallback) {
        if (!reset.get()) {
            if (packet.isInflightPacket()) {
                lossDetectors[packet.getPnSpace().ordinal()].packetSent(packet, sent, packetLostCallback);
                setLossDetectionTimer();
            }
        }
    }

    private boolean ackElicitingInFlight() {
        return Stream.of(lossDetectors).anyMatch(LossDetector::ackElicitingInFlight);
    }

    // TODO never invoked
    public void setReceiverMaxAckDelay(int receiverMaxAckDelay) {
        this.receiverMaxAckDelay = receiverMaxAckDelay;
    }

    public void stopRecovery() {
        if (reset.compareAndSet(false, true)) {
            unschedule();
            scheduler.shutdown();
            Arrays.stream(lossDetectors).forEach(LossDetector::reset);
            scheduler.shutdownNow();
        }
    }

    public void stopRecovery(PnSpace pnSpace) {
        if (!reset.get()) {
            lossDetectors[pnSpace.ordinal()].reset();
            // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-6.2.2
            // "When Initial or Handshake keys are discarded, the PTO and loss detection timers MUST be reset"
            ptoCount.set(0);
            setLossDetectionTimer();
        }
    }

    public long getLost() {
        return Stream.of(lossDetectors).mapToLong(LossDetector::getLost).sum();
    }

    @Override
    public void handshakeStateChangedEvent(HandshakeState newState) {
        if (!reset.get()) {
            HandshakeState oldState = handshakeState.getAndSet(newState);
            if (newState == HandshakeState.Confirmed && oldState != HandshakeState.Confirmed) {
                LogUtils.verbose(TAG, "State is set to " + newState);
                // https://tools.ietf.org/html/draft-ietf-quic-recovery-30#section-6.2.1
                // "A sender SHOULD restart its PTO timer (...), when the handshake is confirmed (...),"
                setLossDetectionTimer();
            }
        }
    }

    @Override
    public void process(AckFrame frame, PnSpace pnSpace, Instant timeReceived) {
        onAckReceived(frame, pnSpace, timeReceived);
    }

    private static class NullScheduledFuture implements ScheduledFuture<Void> {
        @Override
        public int compareTo(Delayed o) {
            return 0;
        }

        @Override
        public long getDelay(TimeUnit unit) {
            return 0;
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return false;
        }

        @Override
        public Void get() throws ExecutionException {
            return null;
        }

        @Override
        public Void get(long timeout, TimeUnit unit) throws ExecutionException, TimeoutException {
            return null;
        }
    }

    private static class PnSpaceTime {
        public final PnSpace pnSpace;
        public final Instant lossTime;

        public PnSpaceTime(PnSpace pnSpace, Instant pnSpaceLossTime) {
            this.pnSpace = pnSpace;
            this.lossTime = pnSpaceLossTime;
        }

        @NonNull
        @Override
        public String toString() {
            return lossTime.toString() + " (in " + pnSpace + ")";
        }
    }
}
