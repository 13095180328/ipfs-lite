package threads.lite;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.security.cert.X509Certificate;

import threads.lite.cid.PeerId;
import threads.lite.host.LiteCertificate;


@RunWith(AndroidJUnit4.class)
public class IpfsSelfSignedTest {
    private static final String TAG = IpfsSelfSignedTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void certificateTest() throws Exception {

        LogUtils.debug(TAG, LiteCertificate.integersToString(LiteCertificate.PREFIXED_EXTENSION_ID));


        assertNotNull(LiteCertificate.getLiteExtension());

        IPFS ipfs = TestEnv.getTestInstance(context);
        assertNotNull(ipfs);


        X509Certificate cert = LiteCertificate.createCertificate(ipfs.getKeys()).x509Certificate();
        assertNotNull(cert);

        PeerId peerId = LiteCertificate.extractPeerId(cert);
        assertNotNull(peerId);
        assertEquals(peerId, ipfs.self());

    }

}
