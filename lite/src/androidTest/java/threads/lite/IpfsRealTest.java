package threads.lite;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;

import threads.lite.cid.Cid;
import threads.lite.cid.Network;
import threads.lite.cid.PeerId;
import threads.lite.cid.Prefix;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsRealTest {
    private static final String TAG = IpfsRealTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_blog_ipfs_io() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession(true)) {
            String link = ipfs.resolveDnsLink("blog.ipfs.io");

            assertNotNull(link);
            assertFalse(link.isEmpty());
            Cid cid = Cid.decode(link.replace(IPFS.IPFS_PATH, ""));

            LogUtils.error(TAG, cid.toString());
            LogUtils.error(TAG, cid.getPrefix().toString());

            Cid node = ipfs.resolveCid(session, cid, Collections.emptyList(),
                    new TimeoutCancellable(60));
            assertNotNull(node);

            List<Link> links = ipfs.links(session, node, false, () -> false);
            assertNotNull(links);
            for (Link lnk : links) {
                LogUtils.info(TAG, lnk.toString());
            }

            node = ipfs.resolveCid(session, node, List.of(IPFS.INDEX_HTML),
                    new TimeoutCancellable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCancellable(30));

            assertNotNull(text);
            assertFalse(text.isEmpty());
        }

    }

    @Test
    public void test_freedomReport() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {
            //Freedom Report ipns://k51qzi5uqu5dlnwjrnyyd6sl2i729d8qjv1bchfqpmgfeu8jn1w1p4q9x9uqit

            String key = "k51qzi5uqu5dlnwjrnyyd6sl2i729d8qjv1bchfqpmgfeu8jn1w1p4q9x9uqit";

            IpnsEntity res = ipfs.resolveName(session, PeerId.decode(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.error(TAG, res.toString());

            Cid cid = ipfs.decodeIpnsData(res);

            Prefix prefix = cid.getPrefix();
            LogUtils.error(TAG, prefix.toString());
            assertTrue(prefix.isSha2556());
            LogUtils.error(TAG, prefix.getType().name());

            Cid root = ipfs.resolveCid(session, cid, Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(root);
            assertEquals(cid, root);

            Cid node = ipfs.resolveCid(session, cid, List.of(IPFS.INDEX_HTML), new TimeoutCancellable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCancellable(30));

            assertNotNull(text);
            TestCase.assertFalse(text.isEmpty());
        }
    }

    @Test
    public void test_unknown() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {

            Cid node = ipfs.resolveCid(session,
                    Cid.decode("QmavE42xtK1VovJFVTVkCR5Jdf761QWtxmvak9Zx718TVr"),
                    Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(node);

            List<Link> links = ipfs.links(session, node, false,
                    new TimeoutCancellable(1));
            assertNotNull(links);
            assertFalse(links.isEmpty());
        }
    }

    @Test
    public void test_unknown_2() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {
            Cid node = ipfs.resolveCid(session,
                    Cid.decode("QmfQiLpdBDbSkb2oySwFHzNucvLkHmGFxgK4oA2BUSwi4t"),
                    Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(node);

            List<Link> links = ipfs.links(session, node, false,
                    new TimeoutCancellable(1));
            assertNotNull(links);
            assertFalse(links.isEmpty());
        }
    }

}
