package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.Connection;
import threads.lite.core.NatType;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsHolepunchTest {

    private static final String TAG = IpfsBootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_holepunch() throws Throwable {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (server.getNatType() != NatType.RESTRICTED_CONE) {
            LogUtils.warning(TAG, "nothing to test here NAT TYPE is not RESTRICTED_CONE");
            return;
        }

        assertTrue(ipfs.hasReservations(server));

        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {

            AtomicBoolean success = new AtomicBoolean(false);
            Set<Reservation> reservations = ipfs.reservations(server);
            assertFalse(reservations.isEmpty());
            for (Reservation reservation : reservations) {

                LogUtils.error(TAG, reservation.toString());
                Multiaddr circuitMultiaddr = reservation.circuitMultiaddr();
                assertNotNull(circuitMultiaddr);

                assertNotNull(circuitMultiaddr.getPeerId());
                assertNotNull(circuitMultiaddr.getRelayId());
                assertTrue(circuitMultiaddr.isCircuitAddress());

                try {
                    Connection conn = dummySession.dial(circuitMultiaddr, Parameters.getDefault());
                    assertNotNull(conn);

                    PeerInfo info = dummy.getHost().getPeerInfo(conn)
                            .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                    assertNotNull(info);
                    LogUtils.error(TAG, info.toString());
                    success.set(true);
                    conn.close();
                    break;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable.getMessage());
                }
            }
            assertTrue(success.get()); // at least one connection success
        }
    }

}