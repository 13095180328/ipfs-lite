package threads.lite;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import threads.lite.blockstore.BlockStoreCache;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.IpnsEntity;
import threads.lite.core.PageStore;
import threads.lite.core.PeerStore;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.lite.core.SwarmStore;
import threads.lite.crypto.Key;
import threads.lite.host.LiteHost;
import threads.lite.utils.Reader;
import threads.lite.utils.ReaderStream;

public class Dummy {

    @NonNull
    private final LiteHost host;
    @NonNull
    private final BlockStoreCache blockStore;

    private Dummy(@NonNull Context context) throws Exception {
        this.blockStore = BlockStoreCache.createInstance(context);
        this.host = new LiteHost(Key.generateKeys(), blockStore,
                new PeerStore() {
                    @Override
                    public List<Peer> getRandomPeers(int limit) {
                        return Collections.emptyList();
                    }


                    @Override
                    public void storePeer(@NonNull Peer peer) {

                    }

                    @Override
                    public void removePeer(Peer peer) {

                    }
                }, new SwarmStore() {
            @Override
            public void storeMultiaddr(@NonNull Multiaddr multiaddr) {

            }

            @Override
            public void removeMultiaddr(@NonNull Multiaddr multiaddr) {

            }

            @Override
            public List<Multiaddr> getMultiaddrs() {
                return Collections.emptyList();
            }
        }, new PageStore() {
            @Override
            public void storePage(@NonNull IpnsEntity page) {

            }

            @Override
            public IpnsEntity getPage(@NonNull PeerId peerId) {
                return null;
            }

            @Override
            public void updatePageContent(@NonNull PeerId peerId, @NonNull Cid cid, @NonNull Date eol) {

            }

            @Override
            public void clear() {

            }

            @Override
            public Cid getPageContent(@NonNull PeerId peerId) {
                return null;
            }
        });
    }

    @NonNull
    public static Dummy getInstance(@NonNull Context context) throws Exception {
        synchronized (Dummy.class) {
            return new Dummy(context);
        }
    }

    @Nullable
    public String getText(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            getToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toString();
        }
    }

    public void storeToOutputStream(@NonNull Session session, @NonNull OutputStream outputStream,
                                    @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        long totalRead = 0L;
        int remember = 0;

        Reader reader = getReader(session, cid, progress);
        long size = reader.getSize();

        do {
            if (progress.isCancelled()) {
                throw new IOException();
            }

            ByteString buffer = reader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();
            if (progress.doProgress()) {
                if (size > 0) {
                    int percent = (int) ((totalRead * 100.0f) / size);
                    if (remember < percent) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        } while (true);
    }

    @NonNull
    public byte[] getData(@NonNull Session session, @NonNull Cid cid,
                          @NonNull Progress progress) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            storeToOutputStream(session, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public PeerId self() {
        return host.self();
    }


    public void clearDatabase() {
        blockStore.close();
    }

    @NonNull
    public Reader getReader(@NonNull Session session, @NonNull Cid cid,
                            @NonNull Cancellable cancellable) throws Exception {
        return Reader.getReader(cancellable, session, cid);
    }

    private void getToOutputStream(@NonNull Session session,
                                   @NonNull OutputStream outputStream,
                                   @NonNull Cid cid,
                                   @NonNull Cancellable cancellable)
            throws Exception {
        try (InputStream inputStream = getInputStream(session, cid, cancellable)) {
            IPFS.copy(inputStream, outputStream);
        }
    }

    @NonNull
    public InputStream getInputStream(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {
        Reader reader = getReader(session, cid, cancellable);
        return new ReaderStream(reader);
    }

    @NonNull
    public LiteHost getHost() {
        return host;
    }

    @NonNull
    public Session createSession() {
        return host.createSession(blockStore, false);
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore) {
        return host.createSession(blockStore, false);
    }
}
