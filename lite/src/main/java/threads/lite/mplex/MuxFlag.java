package threads.lite.mplex;

public enum MuxFlag {
    OPEN,
    DATA,
    CLOSE,
    RESET
}
