package threads.lite.relay;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import circuit.pb.Circuit;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Limit;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.holepunch.HolePunch;
import threads.lite.holepunch.HolePunchInfo;
import threads.lite.host.LiteHost;
import threads.lite.host.LiteStream;
import threads.lite.ident.IdentityService;
import threads.lite.mplex.MuxedStream;
import threads.lite.mplex.MuxedTransport;
import threads.lite.noise.CipherStatePair;
import threads.lite.noise.Handshake;
import threads.lite.noise.Noise;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.utils.DataHandler;

public class RelayService {

    private static final String TAG = RelayService.class.getSimpleName();

    @NonNull
    public static Connection directConnection(@NonNull Session session,
                                              @NonNull Multiaddr address,
                                              @NonNull Parameters parameters)
            throws ExecutionException, InterruptedException, TimeoutException, SocketException, UnknownHostException {

        PeerId peerId = address.getPeerId();

        Multiaddr relayAddress = address.getRelayAddress();
        PeerId relayId = relayAddress.getPeerId();
        Objects.requireNonNull(relayId);

        DatagramSocket socket = new DatagramSocket(LiteHost.nextFreePort());
        Connection connection = session.connect(relayAddress, Parameters.getDefault());

        try {
            PeerInfo peerInfo = IdentityService.getPeerInfo(session.self(), connection)
                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

            if (!peerInfo.hasRelayHop()) {
                throw new ConnectException("No relay hop protocol [abort]");
            }

            Multiaddr observed = peerInfo.getObserved();
            if (observed.isCircuitAddress()) {
                throw new ConnectException("Observed address is circuit address [abort]");
            }

            if (observed.isAnyLocalAddress()) {
                throw new ConnectException("Observed address is local address [abort]");
            }

            // check if observed address has same port then connection [validity check]
            // when ports are not equal, no hole punching is possible [kind of pre-condition]
            // fast symmetric NAT test
            boolean enableHolePunch = Objects.equals(observed.getPort(),
                    connection.getLocalAddress().getPort());

            if (!enableHolePunch) {
                throw new ConnectException("Hole punching not possible [abort]");
            }

            // calculate final observed address for hole punching
            Multiaddr derivedObserved = Multiaddr.create(peerId, observed.getInetAddress(),
                    socket.getLocalPort());
            LogUtils.error(TAG, "Derived observed address " + derivedObserved);


            // check if there is a reservation to the peerId
            // then return a relayed connection, otherwise abort
            // and remove relayed connection from swarm

            HolePunchInfo holePunchInfo = new HolePunchInfo(session, derivedObserved,
                    socket, parameters);

            return directConnectionToPeer(session, connection, holePunchInfo)
                    .get(IPFS.RELAY_CONNECT_TIMEOUT, TimeUnit.SECONDS);
            // now we have a direct connection to the given peerId
        } catch (Throwable throwable) {
            socket.close();
            throw throwable;
        } finally {
            session.closeConnection(connection);
        }
    }

    @NonNull
    public static CompletableFuture<Reservation> reservation(
            @NonNull Server server, @NonNull Multiaddr multiaddr) {

        CompletableFuture<Reservation> reservationFuture = new CompletableFuture<>();

        try {
            Multiaddr observed = server.getObserved();
            if (observed == null) {
                reservationFuture.completeExceptionally(
                        new Exception("No observed address within server [abort]"));
                return reservationFuture;
            }

            Connection connection = server.connect(multiaddr,
                    Parameters.getDefault(IPFS.GRACE_PERIOD_RESERVATION));


            IdentityService.getPeerInfo(server.self(), connection)
                    .whenComplete((peerInfo, throwable) -> {
                        if (throwable != null) {
                            connection.close();
                            reservationFuture.completeExceptionally(throwable);
                        } else {

                            if (!peerInfo.hasRelayHop()) {
                                connection.close();
                                reservationFuture.completeExceptionally(
                                        new Exception("No relay hop protocol [abort]"));
                                return;
                            }

                            getReservation(server, connection, multiaddr)
                                    .whenComplete((reservation, throwable1) -> {
                                        if (throwable1 != null) {
                                            connection.close();
                                            reservationFuture.completeExceptionally(throwable1);
                                        } else {
                                            connection.keepAlive(IPFS.GRACE_PERIOD);
                                            reservationFuture.complete(reservation);
                                        }
                                    });

                        }
                    });
        } catch (Throwable throwable) {
            reservationFuture.completeExceptionally(throwable);
        }
        return reservationFuture;
    }


    public static CompletableFuture<Reservation> getReservation(
            @NonNull Server server, @NonNull Connection connection,
            @NonNull Multiaddr relayAddress) {


        CompletableFuture<Reservation> done = new CompletableFuture<>();

        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void streamTerminated() {
                if (!done.isDone()) {
                    done.completeExceptionally(new Throwable("stream terminated"));
                }
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {

                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.RELAY_PROTOCOL_HOP).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }

                if (Objects.equals(protocol, IPFS.RELAY_PROTOCOL_HOP)) {
                    Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                            .setType(Circuit.HopMessage.Type.RESERVE).build();
                    stream.writeOutput(DataHandler.encode(message))
                            .thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data.array());
                if (msg.getType() == Circuit.HopMessage.Type.STATUS) {
                    if (msg.getStatus() == Circuit.Status.OK) {

                        if (msg.hasReservation()) {
                            Circuit.Reservation reserve = msg.getReservation();

                            Limit limit = getLimit(msg);
                            Reservation reservation = new Reservation(limit,
                                    stream.getConnection(), relayAddress,
                                    Multiaddr.createCircuit(server.self(), relayAddress),
                                    reserve.getExpire());

                            LogUtils.error(TAG, "ipfs swarm connect " +
                                    reservation.circuitMultiaddr());

                            done.complete(reservation);
                        } else {
                            throwable(stream, new Exception("NO RESERVATION"));
                        }

                    } else {
                        throwable(stream, new Exception(msg.getStatus().toString()));
                    }
                } else {
                    throwable(stream, new Exception("NO RESERVATION"));
                }
            }

        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP));
            }
        });

        return done;
    }

    public static CompletableFuture<Connection> directConnectionToPeer(
            @NonNull Session session, @NonNull Connection connection,
            @NonNull HolePunchInfo holePunchInfo) {


        CompletableFuture<Connection> done = new CompletableFuture<>();

        connection.createStream(new StreamHandler() {
            private Noise.NoiseState initiator;

            private Noise.NoiseState getInitiator() throws Exception {
                if (initiator == null) {
                    initiator = Noise.getInitiator(holePunchInfo.getPeerId(), session.getKeys());
                }
                return initiator;
            }

            @Override
            public void throwable(Stream stream, Throwable throwable) {
                LogUtils.error(TAG, throwable);
                done.completeExceptionally(throwable);
            }

            @Override
            public void streamTerminated() {
                if (!done.isDone()) {
                    done.completeExceptionally(new Throwable("stream terminated"));
                }
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {

                LogUtils.error(TAG, "Protocol " + protocol +
                        " streamId " + stream.getStreamId() + " initiator " + stream.isInitiator());

                stream.setAttribute(PROTOCOL, protocol);

                switch (protocol) {
                    case IPFS.MULTISTREAM_PROTOCOL:
                        if (!stream.isInitiator()) {
                            stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL));
                        }
                        break;

                    case IPFS.RELAY_PROTOCOL_HOP:
                        Circuit.Peer dest = Circuit.Peer.newBuilder()
                                .setId(ByteString.copyFrom(holePunchInfo.getPeerId().encoded()))
                                .build();

                        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                                .setType(Circuit.HopMessage.Type.CONNECT)
                                .setPeer(dest)
                                .build();
                        stream.writeOutput(DataHandler.encode(message));
                        break;
                    case IPFS.NOISE_PROTOCOL:
                        stream.writeOutput(Noise.encodeNoiseMessage(getInitiator().getInitalMessage()));
                        LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);
                        stream.setAttribute(TRANSPORT, new Handshake(liteStream.getQuicStream()));
                        LogUtils.error(TAG, "Transport set to Handshake");
                        break;
                    case IPFS.HOLE_PUNCH_PROTOCOL:
                        // nothing to do here
                        break;
                    default:
                        LogUtils.error(TAG, "Ignore " + protocol);
                        stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));

                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {

                String protocol = (String) stream.getAttribute(PROTOCOL);
                LogUtils.error(TAG, "data streamId " + stream.getStreamId() +
                        " protocol " + protocol + " initiator " + stream.isInitiator());
                Objects.requireNonNull(protocol);
                switch (protocol) {
                    case IPFS.RELAY_PROTOCOL_HOP: {
                        Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data.array());
                        Objects.requireNonNull(msg);
                        if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                            throw new Exception(msg.getType().name());
                        }

                        if (msg.getStatus() != Circuit.Status.OK) {
                            throw new Exception(msg.getStatus().name());
                        }

                        Limit limit = RelayService.getLimit(msg);
                        stream.setAttribute(LIMIT, limit);

                        // do directly the punch hole, instead of noise, etc.[IPFS.NOISE_PROTOCOL]
                        stream.writeOutput(DataHandler.encodeProtocols(
                                IPFS.MULTISTREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL));


                        break;
                    }
                    case IPFS.NOISE_PROTOCOL: {

                        Noise.Response response = getInitiator().handshake(data.array());

                        byte[] msg = response.getMessage();
                        if (msg != null) {
                            stream.writeOutput(Noise.encodeNoiseMessage(msg));
                        }


                        CipherStatePair cipherStatePair = response.getCipherStatePair();

                        if (cipherStatePair != null) {

                            if (stream instanceof MuxedStream) {
                                throw new Exception("not excepted stream");
                            }
                            LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);


                            // upgrade connection
                            MuxedTransport muxedTransport = new MuxedTransport(
                                    liteStream.getQuicStream(),
                                    cipherStatePair.getSender(),
                                    cipherStatePair.getReceiver());
                            stream.setAttribute(TRANSPORT, muxedTransport);

                            LogUtils.error(TAG, "Transport set to MuxedTransport");

                            Limit limit = (Limit) stream.getAttribute(LIMIT);
                            Objects.requireNonNull(limit);

                            // now the punch hole can be made
                            MuxedStream muxedStream = MuxedStream.createStream(muxedTransport)
                                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                            muxedStream.writeOutput(DataHandler.encodeProtocols(
                                    IPFS.MULTISTREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL));
                        }

                        break;
                    }
                    case IPFS.HOLE_PUNCH_PROTOCOL: {

                        Multiaddr multiaddr = HolePunch.response(holePunchInfo, stream, data);

                        if (multiaddr != null) {
                            // the creating of a connection should not block, it will otherwise block
                            // all reading of the underlying port
                            Executors.newSingleThreadExecutor().execute(() -> {
                                try {

                                    Connection connection = ConnectionBuilder.connect(
                                            session.getStreamHandler(), multiaddr,
                                            holePunchInfo.getParameters(),
                                            session.getLiteCertificate(),
                                            holePunchInfo.getSocket());
                                    session.addSwarmConnection(connection);

                                    LogUtils.error(TAG, "[A] Success Hole Punching to [B] " +
                                            connection.getRemoteAddress());
                                    done.complete(connection);
                                } catch (Throwable throwable) {
                                    LogUtils.error(TAG,
                                            "[A] Failure Connect Address to [B] " + multiaddr);
                                    done.completeExceptionally(throwable);
                                }
                            });
                        }
                        break;
                    }
                    default:
                        throw new Exception("Protocol " + protocol + " not supported data " + data);

                }

            }

        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP));
            }
        });

        return done;
    }

    public static Limit getLimit(Circuit.HopMessage hopMessage) {
        long limitData = 0;
        long limitDuration = 0;
        Limit.Kind kind = Limit.Kind.LIMITED;
        if (hopMessage.hasLimit()) {
            Circuit.Limit limit = hopMessage.getLimit();
            if (limit.hasData()) {
                limitData = limit.getData();
            }
            if (limit.hasDuration()) {
                limitDuration = limit.getDuration();
            }
        } else {
            kind = Limit.Kind.STATIC;
        }

        return new Limit(limitData, limitDuration, kind);
    }
}
