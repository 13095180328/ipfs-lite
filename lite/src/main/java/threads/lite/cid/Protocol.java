package threads.lite.cid;

import android.util.SparseArray;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import threads.lite.utils.DataHandler;

public enum Protocol implements Tag {

    IP4(Type.IP4, 32, "ip4"),
    IP6(Type.IP6, 128, "ip6"),
    DNS(Type.DNS, -1, "dns"),
    DNS4(Type.DNS4, -1, "dns4"),
    DNS6(Type.DNS6, -1, "dns6"),
    DNSADDR(Type.DNSADDR, -1, "dnsaddr"),
    UDP(Type.UDP, 16, "udp"),
    P2P(Type.P2P, -1, "p2p"),
    P2PCIRCUIT(Type.P2PCIRCUIT, 0, "p2p-circuit"),
    QUIC(Type.QUIC, 0, "quic"),
    QUICV1(Type.QUICV1, 0, "quic-v1");

    public static final String IPV4_REGEX = "\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";
    private static final Map<String, Protocol> byName = new HashMap<>();
    private static final SparseArray<Protocol> byCode = new SparseArray<>();

    static {
        for (Protocol t : Protocol.values()) {
            byName.put(t.getType(), t);
            byCode.put(t.code(), t);
        }
    }

    private final int code, size;
    private final String type;

    Protocol(int code, int size, String type) {
        this.code = code;
        this.size = size;
        this.type = type;
    }


    public static Protocol get(String name) {
        if (byName.containsKey(name))
            return byName.get(name);
        throw new IllegalStateException("No protocol with name: " + name);
    }

    @NonNull
    public static Protocol get(int code) {
        Protocol protocol = byCode.get(code);
        if (protocol == null) {
            throw new IllegalStateException("No protocol with code: " + code);
        }
        return protocol;
    }


    static void putUvarint(byte[] buf, long x) {
        int i = 0;
        while (x >= 0x80) {
            buf[i] = (byte) (x | 0x80);
            x >>= 7;
            i++;
        }
        buf[i] = (byte) x;
    }

    @NonNull
    private static String address(String[] parts) {
        return "/" + String.join("/", parts);
    }

    @NonNull
    public static String toAddress(Tag[] tags) {
        return address(tagsToStrings(tags));
    }

    @NonNull
    private static String[] tagsToStrings(Tag[] tags) {
        String[] result = new String[tags.length];
        for (int i = 0; i < tags.length; i++) {
            result[i] = tags[i].toString();
        }
        return result;
    }

    public static byte[] encoded(Tag[] parts) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            for (Tag part : parts) {
                if (part instanceof Protocol) {
                    Protocol p = (Protocol) part;
                    p.appendCode(outputStream);
                } else if (part instanceof Port) {
                    Port port = (Port) part;
                    int x = port.getPort();
                    outputStream.write(new byte[]{(byte) (x >> 8), (byte) x});
                } else if (part instanceof InetAddress) {
                    InetAddress inetAddress = (InetAddress) part;
                    Objects.requireNonNull(inetAddress);
                    outputStream.write(inetAddress.getAddress());
                } else if (part instanceof Address) {
                    Address address = (Address) part;
                    Objects.requireNonNull(address);
                    byte[] hashBytes = address.getAddress().getBytes();
                    byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(
                            hashBytes.length) + 6) / 7];
                    putUvarint(varint, hashBytes.length);
                    outputStream.write(varint);
                    outputStream.write(hashBytes);
                } else if (part instanceof PeerId) {
                    PeerId peerId = (PeerId) part;
                    Objects.requireNonNull(peerId);
                    byte[] hashBytes = peerId.encoded();

                    byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(
                            hashBytes.length) + 6) / 7];
                    putUvarint(varint, hashBytes.length);
                    outputStream.write(varint);
                    outputStream.write(hashBytes);
                } else {
                    throw new IllegalStateException("Unknown multiaddr tag: " + part.toString());
                }
            }
            return outputStream.toByteArray();
        } catch (Exception exception) {
            throw new IllegalStateException("Error decoding multiaddress: " + toAddress(parts));
        }

    }

    @NonNull
    static List<Tag> encoded(ByteBuffer in) throws Exception {
        List<Tag> parts = new ArrayList<>();
        while (in.hasRemaining()) {
            int code = DataHandler.readUnsignedVariant(in);
            Protocol p = Protocol.get(code);
            parts.add(p);
            if (p.size() == 0)
                continue;

            Tag tag = p.readTag(in);
            Objects.requireNonNull(tag);
            parts.add(tag);
        }
        return parts;
    }

    @NonNull
    static Tag[] encoded(byte[] data) throws Exception {
        List<Tag> parts = encoded(ByteBuffer.wrap(data));
        //noinspection SimplifyStreamApiCallChains
        return parts.stream().toArray(Tag[]::new);
    }

    @NonNull
    static Tag[] parseMultiaddr(PeerId peerId, ByteBuffer in) throws Exception {
        List<Tag> parts = encoded(in);

        // not nice, but it removes the p2p part when peerId is the same
        int size = parts.size();
        if (size >= 2) {
            Tag tag = parts.get(size - 1);
            if (Objects.equals(tag, peerId)) {
                // remove last two entries (p2p and peerId)
                parts.remove(size - 1);
                parts.remove(size - 2);
            }
        }
        //noinspection SimplifyStreamApiCallChains
        return parts.stream().toArray(Tag[]::new);
    }

    public void appendCode(OutputStream out) throws IOException {
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(code) + 6) / 7];
        putUvarint(varint, code);
        out.write(varint);
    }

    public int size() {
        return size;
    }

    public int code() {
        return code;
    }

    public String getType() {
        return type;
    }

    @NonNull
    @Override
    public String toString() {
        return getType();
    }

    @NonNull
    public Tag addressToTag(String addr) throws Exception {

        switch (this) {
            case IP4:
                if (!addr.matches(IPV4_REGEX))
                    throw new IllegalStateException("Invalid IPv4 address: " + addr);
                return new InetAddress(java.net.Inet4Address.getByName(addr).getAddress());
            case IP6:
                return new InetAddress(java.net.Inet6Address.getByName(addr).getAddress());
            case UDP:
                int x = Integer.parseInt(addr);
                return new Port(x);
            case P2P:
                return PeerId.decode(addr);
            case DNS:
            case DNS4:
            case DNS6:
            case DNSADDR: {
                return new Address(addr);
            }
            default:
                throw new IllegalStateException("Unknown multiaddr type: " + name());
        }

    }

    @NonNull
    public Tag readTag(ByteBuffer in) throws Exception {
        int sizeForAddress = sizeForAddress(in);
        byte[] buf;
        switch (this) {
            case IP4:
            case IP6:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return new InetAddress(java.net.InetAddress.getByAddress(buf).getAddress());
            case UDP:
                int a = in.get() & 0xFF;
                int b = in.get() & 0xFF;
                return new Port((a << 8) | b);
            case P2P:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return PeerId.create(buf);
            case DNS:
            case DNS4:
            case DNS6:
            case DNSADDR:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return new Address(new String(buf));
        }
        throw new IllegalStateException("Unimplemented protocol type: " + type);
    }

    private int sizeForAddress(ByteBuffer in) {
        if (size > 0)
            return size / 8;
        if (size == 0)
            return 0;
        return DataHandler.readUnsignedVariant(in);
    }
}
