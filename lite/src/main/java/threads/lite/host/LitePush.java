package threads.lite.host;


import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;

public class LitePush {
    private final Connection connection;
    private final IpnsEntity ipnsEntity;

    public LitePush(Connection connection, IpnsEntity ipnsEntity) {
        this.connection = connection;
        this.ipnsEntity = ipnsEntity;
    }

    public Connection getConnection() {
        return connection;
    }

    public IpnsEntity getIpnsEntity() {
        return ipnsEntity;
    }
}
