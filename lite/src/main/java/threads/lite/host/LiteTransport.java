package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import threads.lite.core.Stream;
import threads.lite.core.Transport;

public final class LiteTransport implements Transport {

    @NonNull
    private final QuicStream stream;

    public LiteTransport(@NonNull QuicStream stream) {
        this.stream = stream;
    }

    @Override
    public Type getType() {
        return Type.PLAIN;
    }

    @Override
    public Stream getStream() {
        return new LiteStream(stream);
    }
}
