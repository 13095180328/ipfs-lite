package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicConnection;

import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.utils.PackageReader;

public final class LiteConnection implements Connection {

    @NonNull
    private final QuicConnection quicConnection;

    public LiteConnection(@NonNull QuicConnection quicConnection) {
        this.quicConnection = quicConnection;
    }

    public void close() {
        quicConnection.close();
    }

    public boolean isConnected() {
        return quicConnection.isConnected();
    }

    @NonNull
    public InetSocketAddress getRemoteAddress() {
        return quicConnection.getRemoteAddress();
    }

    @NonNull
    @Override
    public InetSocketAddress getLocalAddress() {
        return quicConnection.getLocalAddress();
    }

    @NonNull
    @Override
    public PeerId remotePeerId() {
        return (PeerId) Objects.requireNonNull(quicConnection.getAttribute(REMOTE_PEER));
    }

    @NonNull
    public CompletableFuture<Stream> createStream(@NonNull StreamHandler streamHandler) {
        CompletableFuture<Stream> stream = new CompletableFuture<>();

        quicConnection.createStream(new PackageReader(streamHandler),
                        true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS).
                whenComplete((quicStream, throwable) -> {
                    if (throwable != null) {
                        stream.completeExceptionally(throwable);
                    } else {
                        stream.complete(new LiteStream(quicStream));
                    }
                });
        return stream;
    }

    @NonNull
    @Override
    public Multiaddr remoteMultiaddr() {
        return Multiaddr.create(remotePeerId(), getRemoteAddress());
    }

    @Override
    public void keepAlive(int pingInterval) {
        if (quicConnection instanceof QuicClientConnection) {
            ((QuicClientConnection) quicConnection).keepAlive(pingInterval);
        } else {
            throw new IllegalStateException("keep alive only possible on a client connection");
        }
    }

    @Override
    public int getSmoothedRtt() {
        return quicConnection.getSmoothedRtt();
    }

}
