package threads.lite.dht;

import androidx.annotation.NonNull;

import java.math.BigInteger;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.ID;
import threads.lite.cid.Peer;


public final class QueryPeer implements Comparable<QueryPeer> {

    @NonNull
    private final Peer peer;
    @NonNull
    private final BigInteger distance;
    @NonNull
    private final AtomicReference<PeerState> state = new AtomicReference<>(PeerState.PeerHeard);

    private QueryPeer(@NonNull Peer peer, @NonNull BigInteger distance) {
        this.peer = peer;
        this.distance = distance;
    }

    public static QueryPeer create(@NonNull Peer peer, @NonNull ID key) {
        BigInteger distance = QueryPeer.distance(key, peer.getId());
        return new QueryPeer(peer, distance);
    }

    private static byte[] xor(byte[] x1, byte[] x2) {
        byte[] out = new byte[x1.length];

        for (int i = 0; i < x1.length; i++) {
            out[i] = (byte) (0xff & ((int) x1[i]) ^ ((int) x2[i]));
        }
        return out;
    }

    @NonNull
    public static BigInteger distance(@NonNull ID a, @NonNull ID b) {
        byte[] k3 = xor(a.data, b.data);

        // SetBytes interprets buf as the bytes of a big-endian unsigned
        // integer, sets z to that value, and returns z.
        // big.NewInt(0).SetBytes(k3)

        return new BigInteger(k3);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryPeer queryPeer = (QueryPeer) o;
        return peer.equals(queryPeer.peer) && distance.equals(queryPeer.distance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(peer, distance);
    }

    @NonNull
    public PeerState getState() {
        return state.get();
    }

    public void setState(@NonNull PeerState state) {
        this.state.set(state);
    }

    @NonNull
    @Override
    public String toString() {
        return "QueryPeer{" +
                "id=" + peer.getPeerId() +
                ", distance=" + distance +
                ", state=" + state +
                '}';
    }

    @Override
    public int compareTo(QueryPeer o) {
        return distance.compareTo(o.distance);
    }

    @NonNull
    public Peer getPeer() {
        return peer;
    }

}
