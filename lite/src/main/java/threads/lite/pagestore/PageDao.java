package threads.lite.pagestore;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;


@Dao
public interface PageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPage(IpnsEntity page);

    @Query("SELECT * FROM IpnsEntity WHERE peerId = :peerId")
    @TypeConverters(PeerId.class)
    IpnsEntity getPage(PeerId peerId);

    @Query("UPDATE IpnsEntity SET value =:value, sequence = sequence + 1, eol=:eol   WHERE peerId = :peerId")
    @TypeConverters(PeerId.class)
    void update(PeerId peerId, byte[] value, long eol);

    @Query("Select value From IpnsEntity WHERE peerId = :peerId")
    @TypeConverters(PeerId.class)
    byte[] getValue(PeerId peerId);

}
