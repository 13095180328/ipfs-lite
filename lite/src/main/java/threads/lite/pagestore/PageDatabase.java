package threads.lite.pagestore;

import androidx.room.RoomDatabase;

import threads.lite.core.IpnsEntity;

@androidx.room.Database(entities = {IpnsEntity.class}, version = 1, exportSchema = false)
public abstract class PageDatabase extends RoomDatabase {

    public abstract PageDao pageDao();

}
