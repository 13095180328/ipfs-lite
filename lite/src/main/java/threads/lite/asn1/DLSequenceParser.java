package threads.lite.asn1;

import java.io.IOException;

/**
 * Parser class for DL SEQUENCEs.
 */
public class DLSequenceParser implements ASN1SequenceParser {
    private final ASN1StreamParser _parser;

    DLSequenceParser(ASN1StreamParser parser) {
        this._parser = parser;
    }


    /**
     * Return an in memory, encodable, representation of the SEQUENCE.
     *
     * @return a DLSequence.
     * @throws IOException if there is an issue loading the data.
     */
    public ASN1Primitive getLoadedObject()
            throws IOException {
        return DLFactory.createSequence(_parser.readVector());
    }

    /**
     * Return a DLSequence representing this parser and its contents.
     *
     * @return a DLSequence.
     */
    public ASN1Primitive toASN1Primitive() {
        try {
            return getLoadedObject();
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }
}
