package threads.lite.asn1.pkcs;

import threads.lite.asn1.ASN1ObjectIdentifier;

/**
 * pkcs-1 OBJECT IDENTIFIER ::=<p>
 * { iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) 1 }
 */
public interface PKCSObjectIdentifiers {
    /**
     * PKCS#1: 1.2.840.113549.1.1
     */
    ASN1ObjectIdentifier pkcs_1 = new ASN1ObjectIdentifier("1.2.840.113549.1.1");
    /**
     * PKCS#1: 1.2.840.113549.1.1.10
     */
    ASN1ObjectIdentifier id_RSASSA_PSS = pkcs_1.branch("10");
    /**
     * PKCS#1: 1.2.840.113549.1.1.11
     */
    ASN1ObjectIdentifier sha256WithRSAEncryption = pkcs_1.branch("11");
    /**
     * PKCS#1: 1.2.840.113549.1.1.12
     */
    ASN1ObjectIdentifier sha384WithRSAEncryption = pkcs_1.branch("12");
    /**
     * PKCS#1: 1.2.840.113549.1.1.13
     */
    ASN1ObjectIdentifier sha512WithRSAEncryption = pkcs_1.branch("13");
    /**
     * PKCS#1: 1.2.840.113549.1.1.14
     */
    ASN1ObjectIdentifier sha224WithRSAEncryption = pkcs_1.branch("14");


    /**
     * PKCS#9: 1.2.840.113549.1.9
     */
    ASN1ObjectIdentifier pkcs_9 = new ASN1ObjectIdentifier("1.2.840.113549.1.9");

    /**
     * PKCS#9: 1.2.840.113549.1.9.1
     */
    ASN1ObjectIdentifier pkcs_9_at_emailAddress = pkcs_9.branch("1").intern();
    /**
     * PKCS#9: 1.2.840.113549.1.9.2
     */
    ASN1ObjectIdentifier pkcs_9_at_unstructuredName = pkcs_9.branch("2").intern();

    /**
     * PKCS#9: 1.2.840.113549.1.9.8
     */
    ASN1ObjectIdentifier pkcs_9_at_unstructuredAddress = pkcs_9.branch("8").intern();
}

