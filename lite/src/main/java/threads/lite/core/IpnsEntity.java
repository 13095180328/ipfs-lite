package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;

@Entity
public class IpnsEntity {

    @PrimaryKey
    @ColumnInfo(name = "peerId")
    @TypeConverters(PeerId.class)
    @NonNull
    private final PeerId peerId;

    @ColumnInfo(name = "sequence")
    private final long sequence;
    @ColumnInfo(name = "value", typeAffinity = ColumnInfo.BLOB)
    private final byte[] value;
    @ColumnInfo(name = "eol")
    private final long eol;

    public IpnsEntity(@NonNull PeerId peerId, long eol, byte[] value, long sequence) {
        this.peerId = peerId;
        this.eol = eol;
        this.sequence = sequence;
        this.value = value;
    }

    // Note: this is usually the data of an ipns entry
    // default : "/ipfs/ +<encoded cid>
    @NonNull
    public static Cid decodeIpnsData(byte[] data) throws Exception {
        return Cid.decode(new String(data).replaceFirst(IPFS.IPFS_PATH, ""));
    }

    // Transform a cid object into an expected ipns data format
    // Format : "/ipfs/+ <encoded cid>
    // if cid is version 0 : encoding Base58
    // if cid is version 1 : encoding Base32
    public static byte[] encodeIpnsData(@NonNull Cid cid) {
        String path = IPFS.IPFS_PATH + cid;
        return path.getBytes(StandardCharsets.UTF_8);
    }

    // returns the default eol of an ipns entry
    @NonNull
    public static Date getDefaultEol() {
        return Date.from(new Date().toInstant().plus(IPFS.RECORD_EOL));
    }

    @NonNull
    @Override
    public String toString() {
        return "IpnsEntity{" +
                "peerId=" + peerId +
                ", sequence=" + sequence +
                ", eol=" + getEolDate() +
                '}';
    }

    public long getEol() {
        return eol;
    }

    @NonNull
    public PeerId getPeerId() {
        return peerId;
    }

    public long getSequence() {
        return sequence;
    }

    public byte[] getValue() {
        return value;
    }

    @NonNull
    public Date getEolDate() {
        return new Date(getEol());
    }
}