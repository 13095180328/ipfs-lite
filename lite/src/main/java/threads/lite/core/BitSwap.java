package threads.lite.core;

import androidx.annotation.NonNull;

import bitswap.pb.MessageOuterClass;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;

public interface BitSwap {

    @NonNull
    Block getBlock(Cancellable cancellable, Cid cid) throws Exception;


    void receiveMessage(Connection connection, MessageOuterClass.Message bsm) throws Exception;


    void close();

}
