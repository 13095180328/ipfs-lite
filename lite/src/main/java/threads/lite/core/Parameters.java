package threads.lite.core;

import net.luminis.quic.TransportParameters;

import threads.lite.IPFS;

public class Parameters extends TransportParameters {

    private Parameters(int maxIdleTimeoutInSeconds) {
        super(maxIdleTimeoutInSeconds, IPFS.MESSAGE_SIZE_MAX,
                IPFS.MAX_STREAMS, 0);

    }

    public static Parameters getDefault() {
        return new Parameters(IPFS.GRACE_PERIOD);
    }

    public static Parameters getDefault(int maxIdleTimeoutInSeconds) {
        return new Parameters(maxIdleTimeoutInSeconds);
    }

}
