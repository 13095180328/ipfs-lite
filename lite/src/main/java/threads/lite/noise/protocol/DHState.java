/*
 * Copyright (C) 2016 Southern Storm Software, Pty Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package threads.lite.noise.protocol;

import com.google.crypto.tink.subtle.X25519;

import java.security.InvalidKeyException;
import java.util.Arrays;

/**
 * Interface to a Diffie-Hellman algorithm for the Noise protocol.
 */
public class DHState implements Destroyable {


    private byte[] publicKey;
    private byte[] privateKey;
    private int mode;

    /**
     * Constructs a new Diffie-Hellman object for Curve25519 (based on google tink library).
     */
    public DHState() {
        publicKey = new byte[32];
        privateKey = new byte[32];
        mode = 0;
    }

    /**
     * Gets the length of public keys for this algorithm.
     *
     * @return The length of public keys in bytes.
     */
    @SuppressWarnings("SameReturnValue")
    public int getPublicKeyLength() {
        return 32;
    }

    /**
     * Gets the length of shared keys for this algorithm.
     *
     * @return The length of shared keys in bytes.
     */
    @SuppressWarnings("SameReturnValue")
    public int getSharedKeyLength() {
        return 32;
    }

    /**
     * Generates a new random keypair.
     */
    public void generateKeyPair() throws InvalidKeyException {
        privateKey = X25519.generatePrivateKey();
        publicKey = X25519.publicFromPrivate(privateKey);
        mode = 0x03;
    }

    /**
     * Gets the public key associated with this object.
     *
     * @param key    The buffer to copy the public key to.
     * @param offset The first offset in the key buffer to copy to.
     */
    public void getPublicKey(byte[] key, int offset) {
        System.arraycopy(publicKey, 0, key, offset, 32);
    }

    /**
     * Sets the public key for this object.
     *
     * @param key    The buffer containing the public key.
     * @param offset The first offset in the buffer that contains the key.
     *               <p>
     *               If this object previously held a key pair, then this function
     *               will change it into a public key only object.
     */
    public void setPublicKey(byte[] key, int offset) {
        System.arraycopy(key, offset, publicKey, 0, 32);
        Arrays.fill(privateKey, (byte) 0);
        mode = 0x01;
    }

    /**
     * Clears the key pair.
     */
    public void clearKey() {
        NoiseUtility.destroy(publicKey);
        NoiseUtility.destroy(privateKey);
        mode = 0;
    }

    /**
     * Determine if this object contains a public key.
     *
     * @return Returns true if this object contains a public key,
     * or false if the public key has not yet been set.
     */
    public boolean hasPublicKey() {
        return (mode & 0x01) != 0;
    }

    /**
     * Determine if this object contains a private key.
     *
     * @return Returns true if this object contains a private key,
     * or false if the private key has not yet been set.
     */
    public boolean hasPrivateKey() {
        return (mode & 0x02) != 0;
    }

    /**
     * Determine if the public key in this object is the special null value.
     *
     * @return Returns true if the public key is the special null value,
     * or false otherwise.
     */
    public boolean isNullPublicKey() {
        if ((mode & 0x01) == 0)
            return false;
        int temp = 0;
        for (int index = 0; index < 32; ++index)
            temp |= publicKey[index];
        return temp == 0;
    }

    /**
     * Performs a Diffie-Hellman calculation with this object as the private key.
     *
     * @param sharedKey Buffer to put the shared key into.
     * @param publicDH  Object that contains the public key for the calculation.
     * @throws IllegalArgumentException The publicDH object is not the same
     *                                  type as this object, or one of the objects does not contain a valid key.
     */
    public void calculate(byte[] sharedKey, DHState publicDH) throws InvalidKeyException {
        byte[] shared = X25519.computeSharedSecret(privateKey, publicDH.publicKey);
        System.arraycopy(shared, 0, sharedKey, 0, shared.length);
        //Curve25519.eval(sharedKey, privateKey, publicDH.publicKey);
    }

    /**
     * Copies the key values from another DH object of the same type.
     *
     * @param other The other DH object to copy from
     * @throws IllegalStateException The other DH object does not have
     *                               the same type as this object.
     */
    public void copyFrom(DHState other) {
        if (other == this)
            return;

        System.arraycopy(other.privateKey, 0, privateKey, 0, 32);
        System.arraycopy(other.publicKey, 0, publicKey, 0, 32);
        mode = other.mode;
    }


    @Override
    public void destroy() {
        clearKey();
    }
}
